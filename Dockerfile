FROM node:14-alpine as builder 
WORKDIR /frontend
# ENV PATH /frontend/node_modules/.bin:$PATH

COPY package.json /frontend
# COPY package-lock.json /frontend
COPY yarn.lock /frontend
RUN apk add --update yarn
RUN yarn install 
COPY . . 
# RUN npm fun && npm audit fix --force
RUN npx browserslist@latest --update-db

RUN yarn build


FROM nginx:stable-alpine
COPY --from=builder /frontend/build /usr/share/nginx/html
COPY nginx/nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 8890
CMD ["nginx", "-g", "daemon off;"]

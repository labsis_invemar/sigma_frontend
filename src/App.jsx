import React,{useState} from "react";
import "./assets/style/App.css";
import "./assets/style/Barra.css";
import Nav from "./components/navbar";
import Men from "./components/menu";
// import Mapa from "./components/map";
import Map from "./components/MapaArcgis";
import "../src/index.css";

import {MapaProvider} from "./context/MapaContext"
function App() {
  const [estado,SetEstado] = useState(true);
  
  const handleActive=() => {
  SetEstado(!estado);

  if(estado === true){
    console.log("Verdadero desde el padre");
  }
  if (estado === false) {
    console.log("Falso  desde el padre");
  }
};
  return (
    <div className="container mw-100">
      <div className="Barra StyleNav StyleBorder">
      <Nav />
      </div>

      <div className="Mapa">
      <MapaProvider>
          <Men handle={handleActive} SetEstado={SetEstado}/>
             {/*<!--<Mapa estado={estado}/>-->*/} 
          <Map/>
        </MapaProvider>
      </div>
    </div>
  );
}

export default App;

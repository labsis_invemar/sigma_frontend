import React,{useContext} from "react";
import Form from "react-bootstrap/Form";
import {MapaContext} from '../../context/MapaContext'

function FSeriesCGSM({handle}) {
  const {SetSerieTiempo}=useContext(MapaContext)
  const {SetEst}=useContext(MapaContext)

  const presione=(e)=>{
    if (e.target.checked === true) {
      SetSerieTiempo(true);
    }else{
      SetSerieTiempo(false);
    }
  }

  return (
    <>
    <br/>
    <br />
      <p align="left">
        Este módulo permite la visualización de la series de tiempo de la CGSM registradas en
        SIGMA.
      </p>
    <p>
        <a 
            href="https://invemar.maps.arcgis.com/apps/Cascade/index.html?appid=de012f87befb472c9a5e7794206d1230"
            target="_blank">
            Para más Información, haga clic aqui.
        </a>
    </p>
    
      <Form>
        {["checkbox"].map((type) => (
          <div key={`default`} className="mb-3">
            <Form.Check type={type} id={"Check1"} label={"Activar visualización"} onClick={presione} />
          </div>
        ))}
      </Form>
    
    </>
  );
}

export default FSeriesCGSM;
import React,{useContext} from "react";
import Form from "react-bootstrap/Form";
import {MapaContext} from '../../context/MapaContext'

function FCaracterizacion({handle}) {
  const {setData}=useContext(MapaContext)
  const { Setcaracterizacion } = useContext(MapaContext);
 /*  const presione=(e)=>{
    setData([e]);
    handle(e)
  } */

  const presione=(e)=>{
    if (e.target.checked === true) {
      Setcaracterizacion(true)
    }else{
      Setcaracterizacion(false)
    }
  }

  return (
    <>
    <br/>
    <br />
      <p align="left">
        Este módulo permite la visualización de las parcelas registradas en
        SIGMA con sus detalles y descripciones, así como su disponibilidad de
        datos.
      </p>
      <Form>
        {["checkbox"].map((type) => (
          <div key={`default`} className="mb-3">
            <Form.Check type={type} id={"Check1"} label={"Activar visualización"} onClick={presione} />
          </div>
        ))}
      </Form>
    
    </>
  );
}

export default FCaracterizacion;

import React, { useEffect,useReducer,useContext} from "react";

import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import {url_departamento_estructura,url_municipio_estructura,url_estacion_estructura,url_parcela_estructura,url_especie_estructura, url_datos_estructura_especie} from '../api';
import {MapaContext} from '../../context/MapaContext'


const initialState = {
  data_siembra:[],

  data_departamento:[],
  data_municipio:[],
  data_estacion:[],
  data_parcela:[],
  data_especie:[],
  data_variable:[],
  data_info:[],

  id_estacion:null,
  id_departamento:null,
  id_municipio:null,
  id_parcela:null,
  id_especie:null,
  id_variable:null,
  id_info:null,
  id_estado:false,

  id_departamento_error:false,
  id_municipio_error:false,
  id_estacion_error:false,
  id_parcela_error:false,
  id_especie_error:false,
  id_variable_error:false,
}

// constantes para switch
const DEPARTAMENTO = "DEPARTAMENTO"
const MUNICIPIO = "MUNICIPIO"
const ESTACION = "ESTACION"
const PARCELA = "PARCELA"
const ESPECIE = "ESPECIE"
const VARIABLE = "VARIABLE"
const INFO = "INFO"
const LIMPIAR = "LIMPIAR"

const ID_ESTACION = "ID_ESTACION"
const ID_DEPARTAMENTO = "ID_DEPARTAMENTO"
const ID_MUNICIPIO = "ID_MUNICIPIO"
const ID_PARCELA = "ID_PARCELA"
const ID_ESPECIE = "ID_ESPECIE"
const ID_VARIABLE = "ID_VARIABLE"
const ID_INFO = "ID_INFO"
const ID_ESTADO = "ID_ESTADO"

const ID_DEPARTAMENTO_ERROR = "ID_DEPARTAMENTO_ERROR"
const ID_MUNICIPIO_ERROR = "ID_MUNICIPIO_ERROR"
const ID_ESTACION_ERROR = "ID_ESTACION_ERROR"
const ID_PARCELA_ERROR = "ID_PARCELA_ERROR"
const ID_ESPECIE_ERROR = "ID_ESPECIE_ERROR"
const ID_VARIABLE_ERROR = "ID_VARIABLE_ERROR" 




const reducer = (state,action) => {
  
  switch (action.type) {
    case DEPARTAMENTO:
      return {...state,data_departamento:action.payload}

    case MUNICIPIO:
      return {...state,data_municipio:action.payload}
  
    case ESTACION:
      return {...state,data_estacion:action.payload}

    case PARCELA:
      return {...state,data_parcela:action.payload}
    
    case ESPECIE:
      return {...state,data_especie:action.payload}
    
    case VARIABLE:
      return {...state,data_variable:action.payload}

    case INFO:
      return {...state,data_info:action.payload}
    
    case LIMPIAR:
      return {
        data_municipio: [],
        data_estacion:[],
        data_parcela:[],
        data_especie:[],
        data_variable:[],
        data_info:[],
        
        id_municipio:null,
        id_estacion:null,
        id_parcela:null,
        id_especie:null,
        id_variable:null,
        id_info:null,
        id_estado:false
      }

    
    case ID_DEPARTAMENTO:
      return {...state,id_departamento:action.payload , data_municipio : []} 
      
    case ID_MUNICIPIO:
      return {...state,id_municipio:action.payload , data_estacion : []} 
        
    case ID_ESTACION:
      return {...state,id_estacion:action.payload, data_parcela: []}

    case ID_PARCELA:
      return {...state,id_parcela:action.payload, data_especie: []}
    
    case ID_ESPECIE:
      return {...state,id_especie:action.payload, data_variable: []}
    
    case ID_VARIABLE:
      return {...state,id_variable:action.payload}
      
    case ID_INFO:
      return {...state,id_info:action.payload}
    
    case ID_ESTADO:
      return {...state,id_estado:action.payload}
    
    
      case ID_DEPARTAMENTO_ERROR:
       return {...state,id_departamento_error:action.payload}
    
    case ID_MUNICIPIO_ERROR:
       return {...state,id_municipio_error:action.payload}
    
    case ID_ESTACION_ERROR:
      return {...state,id_estacion_error:action.payload}
    
    case ID_PARCELA_ERROR:
      return {...state,id_parcela_error:action.payload}
    
    case ID_ESPECIE_ERROR:
      return {...state,id_especie_error:action.payload}
    
    case ID_VARIABLE_ERROR:
      return {...state,id_variable_error:action.payload}
  
    default:
      return state;
  }

}





function FEstructura(props) {

  const [state, dispatch] = useReducer(reducer,initialState);
  const {setData}=useContext(MapaContext)
  const {SetEst}=useContext(MapaContext)
  const {data}=useContext(MapaContext)
  const {SetParcela}=useContext(MapaContext)
  const {SetEstadoMapa}=useContext(MapaContext)




  useEffect(() => {
    if (data === LIMPIAR) {
      SetEst('Noest');
      dispatch({type:LIMPIAR})
    }
  }, [data])
  


//busca departamentos
  useEffect(()=>{
    fetch(url_departamento_estructura,{
      crossdomain: true,
    }).then(resp=>resp.json())
    .then(resp =>dispatch({type:DEPARTAMENTO,payload:resp}))
  },[state.id_departamento])
  // console.log(JSON.stringify(state.data_departamento))

//busca municipio
  useEffect(()=>{
       if(state.id_departamento){
          fetch(url_municipio_estructura(state.id_departamento),{
          crossdomain: true,
        }).then(resp=>resp.json())
        .then(resp =>dispatch({type:MUNICIPIO,payload:resp}) )
    }
  },[state.id_departamento])

//busca estacion
useEffect(()=>{
  if(state.id_departamento){
     fetch(url_estacion_estructura(state.id_municipio),{
     crossdomain: true,
   }).then(resp=>resp.json())
   .then(resp =>dispatch({type:ESTACION,payload:resp}) )
}
},[state.id_municipio])
  
//busca parcelas 
  useEffect(()=>{
    
    if(state.id_estacion){
        fetch(url_parcela_estructura(state.id_estacion),{
          crossdomain: true,
        }).then(res=>res.json())
        .then(res =>dispatch({type:PARCELA,payload:res}) )
    }
  },[state.id_estacion])


    //busca especie 
    useEffect(()=>{
      if(state.id_parcela){
          fetch(url_especie_estructura(state.id_parcela),{
            crossdomain: true,
          }).then(res=>res.json())
          .then(res =>dispatch({type:ESPECIE,payload:res}) )
      }
    },[state.id_parcela])


    //busca info 
    useEffect(()=>{
      if(state.id_info){
          fetch(url_datos_estructura_especie(state.id_parcela,state.id_especie,state.id_variable),{
            crossdomain: true,
          }).then(res=>res.json())
          .then(res =>{
            //console.log("res",res);
            setData(res);
            SetParcela(state.id_parcela);
            SetEst('est');
            SetEstadoMapa(true);//aca va true
            dispatch({type:INFO,payload:res});
        }).then(dispatch({type:ID_ESTADO,payload:true}))
      }
    },[state.id_info])


    






  //imprime los estados por consola  
  // const hadleClick = (()=>{
  //   /*console.log(state)
  //   /*SetEst(true);*/
  //   /*dispatch({type:LIMPIAR})*/
  // })

  //Función para verificar los campos y consultar info
  const handleSubmit = (()=>{
    if (state.id_departamento && state.id_estacion && state.id_parcela && state.id_especie && state.id_variable) {
      dispatch({type:ID_DEPARTAMENTO_ERROR,payload:false})
      dispatch({type:ID_ESTACION_ERROR,payload:false})
      dispatch({type:ID_PARCELA_ERROR,payload:false})
      dispatch({type:ID_ESPECIE_ERROR,payload:false})
      dispatch({type:ID_VARIABLE_ERROR,payload:false})
      dispatch({type:ID_INFO,payload:1})
    }
    else{
      if (!state.id_departamento) {
        dispatch({type:ID_DEPARTAMENTO_ERROR,payload:true})
        dispatch({type:ID_ESTACION_ERROR,payload:true})
        dispatch({type:ID_PARCELA_ERROR,payload:true})
        dispatch({type:ID_ESPECIE_ERROR,payload:true})
        dispatch({type:ID_VARIABLE_ERROR,payload:true})
      }
        else
          if (!state.id_estacion){
            dispatch({type:ID_DEPARTAMENTO_ERROR,payload:false})
            dispatch({type:ID_ESTACION_ERROR,payload:true})
            dispatch({type:ID_PARCELA_ERROR,payload:true})
            dispatch({type:ID_ESPECIE_ERROR,payload:true})
            dispatch({type:ID_VARIABLE_ERROR,payload:true})
          }
          else
            if (!state.id_parcela){
              dispatch({type:ID_DEPARTAMENTO_ERROR,payload:false})
              dispatch({type:ID_ESTACION_ERROR,payload:false})
              dispatch({type:ID_PARCELA_ERROR,payload:true})
              dispatch({type:ID_ESPECIE_ERROR,payload:true})
              dispatch({type:ID_VARIABLE_ERROR,payload:true})
            }
            else
              if (!state.id_especie){
                dispatch({type:ID_DEPARTAMENTO_ERROR,payload:false})
                dispatch({type:ID_ESTACION_ERROR,payload:false})
                dispatch({type:ID_PARCELA_ERROR,payload:false})
                dispatch({type:ID_ESPECIE_ERROR,payload:true})
                dispatch({type:ID_VARIABLE_ERROR,payload:true})
              }
              else
                if (!state.id_variable) {
                  dispatch({type:ID_DEPARTAMENTO_ERROR,payload:false})
                  dispatch({type:ID_ESTACION_ERROR,payload:false})
                  dispatch({type:ID_PARCELA_ERROR,payload:false})
                  dispatch({type:ID_ESPECIE_ERROR,payload:false})
                  dispatch({type:ID_VARIABLE_ERROR,payload:true})
                }
      
    }

  })




  return (
    <>
      <Form>
        <Form.Group controlId="exampleForm.ControlSelect1">
          <Form.Label>Departamento</Form.Label>
          <Form.Control as="select" onChange={(e)=> dispatch({type:ID_DEPARTAMENTO,payload:e.target.value})} defaultValue={'DEFAULT'} disabled={state.id_estado} isInvalid={state.id_departamento_error}>
            <option value="DEFAULT">Opciones..</option>
            {
              state.data_departamento && state.data_departamento.map(dep => <option key={dep.id_departamento} value={dep.id_departamento} >{dep.departamento}</option>)
            }
          </Form.Control>
        </Form.Group>

        <Form.Group controlId="exampleForm.ControlSelect2">
          <Form.Label>Municipio</Form.Label>
          <Form.Control as="select" onChange={(e)=> dispatch({type:ID_MUNICIPIO,payload:e.target.value})} defaultValue={'DEFAULT'} disabled={state.id_estado} isInvalid={state.id_municipio_error}>
          <option value="DEFAULT">Opciones..</option>
          {
           state.data_municipio && state.data_municipio.map(items => <option key={items.id_municipio} value ={items.id_municipio}> {items.municipio}</option>)
          } 
          </Form.Control>
        </Form.Group>

        <Form.Group controlId="exampleForm.ControlSelect3">
          <Form.Label>Estación</Form.Label>
          <Form.Control as="select" onChange={(e)=> dispatch({type:ID_ESTACION,payload:e.target.value})} defaultValue={'DEFAULT'} disabled={state.id_estado} isInvalid={state.id_estacion_error}>
          <option value="DEFAULT">Opciones..</option>
          {
           state.data_estacion && state.data_estacion.map(items => <option key={items.id_estacion} value ={items.id_estacion}> {items.estacion}</option>)
          } 
          </Form.Control>
        </Form.Group>

        <Form.Group controlId="exampleForm.ControlSelect4">
          <Form.Label>Parcela</Form.Label>
          <Form.Control as="select"  onChange={(e)=> dispatch({type:ID_PARCELA,payload:e.target.value})} defaultValue={'DEFAULT'} disabled={state.id_estado} isInvalid={state.id_parcela_error}>
            <option value="DEFAULT">Opciones..</option>
          {
           state.data_parcela && state.data_parcela.map(items => <option key={items.id_parcela} value ={items.id_parcela}> {items.parcela}</option>)
          } 
          </Form.Control>
        </Form.Group>
        <Form.Group controlId="exampleForm.ControlSelect5">
          <Form.Label>Especie</Form.Label>
          <Form.Control as="select" onChange={(e)=> dispatch({type:ID_ESPECIE,payload:e.target.value})} defaultValue={'DEFAULT'} disabled={state.id_estado} isInvalid={state.id_especie_error}>
            <option value="DEFAULT">Opciones..</option>
           { state.data_especie.length > 0 &&  <option value="total" >Todas las especies</option>}
          {
           state.data_especie && state.data_especie.map(items => <option key={items.id_especie} value ={items.id_especie}> {items.especie}</option>)
          }  
          </Form.Control>
        </Form.Group>
        <Form.Group controlId="exampleForm.ControlSelect6">
          <Form.Label>Variable</Form.Label>
          <Form.Control as="select" onChange={(e)=> dispatch({type:ID_VARIABLE,payload:e.target.value})} defaultValue={'DEFAULT'} disabled={state.id_estado} isInvalid={state.id_variable_error}>
            <option value="DEFAULT">Opciones..</option>
            { state.data_especie.length > 0 && <option key={1} value="densidad_estacion">Densidad Estación -- ind/ha</option>}
            { state.data_especie.length > 0 && <option key={2} value="densidad_absoluta_especie"> Densidad Especie -- ind/ha</option>}
            { state.data_especie.length > 0 && <option key={3} value="abundancia_relativa_especie">Abundancia Relativa</option>}
            { state.data_especie.length > 0 && <option key={4} value="area_basal_especie">Area Basal Especie -- m²/ha</option>}
            { state.data_especie.length > 0 && <option key={5} value="area_basal_estacion">Area Basal Estación -- m²/ha</option>}
            { state.data_especie.length > 0 && <option key={6} value="dominancia_relativa">Dominancia Relativa</option>}
            { state.data_especie.length > 0 && <option key={7} value="frecuencia_relativa_especie">Frecuencia Relativa Especie</option>}
            { state.data_especie.length > 0 && <option key={8} value="indice_valor_importancia">Indice de valor importancia</option>}
          </Form.Control>
          {/* <p>hola</p> */}
        </Form.Group>

        
        <br />
          

          
          <div className="btn-group mx-2" role="group" aria-label="primer boton">
            <Button variant="outline-success" type="button" value="1" onClick={handleSubmit}>
              Buscar
            </Button>
          </div>
          {/* <div className="btn-group mx-2" role="group" aria-label="segundo boton">
            <Button variant="outline-primary" type="button" value="1" onClick={hadleClick}>
              Limpiar
            </Button>
          </div> */}

      </Form>
    </>
  );
}
export default FEstructura;
import React,{ useEffect,useReducer,useContext} from 'react';
import {url_departamento_fq,url_municipio_fq,url_estacion_fq,url_datos_fq_estacion} from '../api';
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import {MapaContext} from '../../context/MapaContext';



//estados iniciales
const initialState = {
  data_departamento:[],
  data_municipio:[],
  data_estacion:[],
  data_variable:[],
  data_info:[],

  id_estacion:null,
  id_departamento:null,
  id_municipio:null,
  id_variable:null,
  id_info:null,
  id_estado:false,

  id_departamento_error:false,
  id_municipio_error:false,
  id_estacion_error:false,
  id_variable_error:false,
}

//Constantes para switch

const DEPARTAMENTO = "DEPARTAMENTO"
const MUNICIPIO = "MUNICIPIO"
const ESTACION = "ESTACION"
const VARIABLE = "VARIABLE"
const INFO = "INFO"
const LIMPIAR = "LIMPIAR"

const ID_ESTACION = "ID_ESTACION"
const ID_DEPARTAMENTO = "ID_DEPARTAMENTO"
const ID_MUNICIPIO = "ID_MUNICIPIO"
const ID_VARIABLE = "ID_VARIABLE"
const ID_INFO = "ID_INFO"
const ID_ESTADO = "ID_ESTADO"

const ID_DEPARTAMENTO_ERROR = "ID_DEPARTAMENTO_ERROR"
const ID_MUNICIPIO_ERROR = "ID_MUNICIPIO_ERROR"
const ID_ESTACION_ERROR = "ID_ESTACION_ERROR"
const ID_VARIABLE_ERROR = "ID_VARIABLE_ERROR" 


const reducer = (state,action) => {
  
  switch (action.type) {
    case DEPARTAMENTO:
      return {...state,data_departamento:action.payload}

    case MUNICIPIO:
      return {...state,data_municipio:action.payload}
  
    case ESTACION:
      return {...state,data_estacion:action.payload}

    case VARIABLE:
      return {...state,data_variable:action.payload}

    case INFO:
      return {...state,data_info:action.payload}
    
    case LIMPIAR:
      return {
        data_estacion:[],
        data_parcela:[],
        data_info:[],
      
        id_estacion:null,
        id_variable:null,
        id_info:null,
        id_estado:false
      }

    
    case ID_DEPARTAMENTO:
      return {...state,id_departamento:action.payload , data_estacion : []} 

    case ID_MUNICIPIO:
      return {...state,id_municipio:action.payload , data_estacion : []}     
    
      case ID_ESTACION:
      return {...state,id_estacion:action.payload, data_parcela: []}
    
    case ID_VARIABLE:
      return {...state,id_variable:action.payload}
      
    case ID_INFO:
      return {...state,id_info:action.payload}
    
    case ID_ESTADO:
      return {...state,id_estado:action.payload}
    
    
    case ID_DEPARTAMENTO_ERROR:
      return {...state,id_departamento_error:action.payload}
    
    case ID_MUNICIPIO_ERROR:
      return {...state,id_municipio_error:action.payload}      

    case ID_ESTACION_ERROR:
      return {...state,id_estacion_error:action.payload}
    
    
    case ID_VARIABLE_ERROR:
      return {...state,id_variable_error:action.payload}
  
    default:
      return state;
  }

}


function FFisicoQuimico() {
  const [state, dispatch] = useReducer(reducer,initialState);
  const {SetEst}=useContext(MapaContext)
  const {setData}=useContext(MapaContext)
  const {data}=useContext(MapaContext)
  const {SetmultipleParcelas,SetParcela}=useContext(MapaContext);
  
  useEffect(() => {
    if (data === LIMPIAR) {
      console.log(state)
      SetEst('Nofq');
      dispatch({type:LIMPIAR})
    }
  }, [])


  //busca departamentos
  useEffect(()=>{
    fetch(url_departamento_fq,{
      crossdomain: true,
    }).then(resp=>resp.json())
    .then(resp =>dispatch({type:DEPARTAMENTO,payload:resp}))
  },[state.id_departamento])

 //busca municipios
 useEffect(()=>{
  console.log('iddep: ',state.id_departamento)
     if(state.id_departamento){
        fetch(url_municipio_fq(state.id_departamento),{
        crossdomain: true,
      }).then(resp=>resp.json())
      .then(resp =>{
        console.log('respmm',resp)
        dispatch({type:MUNICIPIO,payload:resp})
      } )
  }
},[state.id_departamento])  

  //busca estacion
  useEffect(()=>{
    console.log('iddep: ',state.id_municipio)
       if(state.id_municipio){
          fetch(url_estacion_fq(state.id_municipio),{
          crossdomain: true,
        }).then(resp=>resp.json())
        .then(resp =>dispatch({type:ESTACION,payload:resp}) )
    }
  },[state.id_municipio])



  //busca info 
  useEffect(()=>{
    if(state.id_info){
        fetch(url_datos_fq_estacion(state.id_estacion,state.id_variable),{
          crossdomain: true,
      }).then(res=>res.json())
         .then(res =>{
           setData(res);
           SetParcela(state.id_estacion);
           SetEst('Nofq')
           SetmultipleParcelas(true);
           //SetEstadoMapa(true);
           dispatch({type:INFO,payload:res});
       }).then(dispatch({type:ID_ESTADO,payload:true}))
     }
  },[state.id_info])

  

  //Función para verificar los campos y consultar info
  const handleSubmit = (()=>{
    if (state.id_departamento && state.id_estacion && state.id_variable) {
      dispatch({type:ID_DEPARTAMENTO_ERROR,payload:false})
      dispatch({type:ID_ESTACION_ERROR,payload:false})
      dispatch({type:ID_VARIABLE_ERROR,payload:false})
      dispatch({type:ID_INFO,payload:1})
    }
    else{
      if (!state.id_departamento) {
        dispatch({type:ID_DEPARTAMENTO_ERROR,payload:true})
        dispatch({type:ID_ESTACION_ERROR,payload:true})
        dispatch({type:ID_VARIABLE_ERROR,payload:true})
      }
        else
          if (!state.id_estacion){
            dispatch({type:ID_DEPARTAMENTO_ERROR,payload:false})
            dispatch({type:ID_ESTACION_ERROR,payload:true})
            dispatch({type:ID_VARIABLE_ERROR,payload:true})
          }
          else
                if (!state.id_variable) {
                  dispatch({type:ID_DEPARTAMENTO_ERROR,payload:false})
                  dispatch({type:ID_ESTACION_ERROR,payload:false})
                  dispatch({type:ID_VARIABLE_ERROR,payload:true})
                }
      
    }

  })




  return(
        <>
          <br />
                <Form>
                  <Form.Group controlId="exampleForm.ControlSelect1">
                    <Form.Label>Departamento</Form.Label>
                    <Form.Control className="animate__animated animate__fadeIn" as="select" onChange={(e)=> dispatch({type:ID_DEPARTAMENTO,payload:e.target.value})} defaultValue={'DEFAULT'} disabled={state.id_estado} isInvalid={state.id_departamento_error}>
                      <option value="DEFAULT">Opciones..</option>
                      {
                        state.data_departamento && state.data_departamento.map(dep => <option key={dep.id_departamento} value={dep.id_departamento} >{dep.departamento}</option>)
                      }
                    </Form.Control>
                  </Form.Group>
                  <Form.Group controlId="exampleForm.ControlSelect2">
                    <Form.Label>Municipio</Form.Label>
                    <Form.Control className="animate__animated animate__fadeIn" as="select" onChange={(e)=> dispatch({type:ID_MUNICIPIO,payload:e.target.value})} defaultValue={'DEFAULT'} disabled={state.id_estado} isInvalid={state.id_municipio_error}>
                    <option value="DEFAULT">Opciones..</option>
                    {
                      state.data_municipio && state.data_municipio.map(items => <option key={items.id_municipio} value ={items.id_municipio}> {items.municipio}</option>)
                    } 
                    </Form.Control>
                  </Form.Group>

                  <Form.Group controlId="exampleForm.ControlSelect3">
                    <Form.Label>Estación</Form.Label>
                    <Form.Control className="animate__animated animate__fadeIn" as="select" onChange={(e)=> dispatch({type:ID_ESTACION,payload:e.target.value})} defaultValue={'DEFAULT'} disabled={state.id_estado} isInvalid={state.id_estacion_error}>
                    <option value="DEFAULT">Opciones..</option>
                    {
                      state.data_estacion && state.data_estacion.map(items => <option key={items.id_estacion} value ={items.id_estacion}> {items.estacion}</option>)
                    } 
                    </Form.Control>
                  </Form.Group>

                  <Form.Group controlId="exampleForm.ControlSelect4">
                    <Form.Label>Variable</Form.Label>
                    <Form.Control className="animate__animated animate__fadeIn" as="select" onChange={(e)=> dispatch({type:ID_VARIABLE,payload:e.target.value})} defaultValue={'DEFAULT'} disabled={state.id_estado} isInvalid={state.id_variable_error}>
                      <option value="DEFAULT">Opciones..</option>
                      { state.data_estacion.length > 0 && <option key={'pro_nivel_agua'} value="pro_nivel_agua">Promedio nivel agua</option>}
                      { state.data_estacion.length > 0 && <option key={'pro_sal_1'} value="pro_sal_1"> Promedio salinidad - 1m</option>}
                      { state.data_estacion.length > 0 && <option key={'pro_sal_05'} value="pro_sal_05">Promedio salinidad - 0.5m</option>}
                      { state.data_estacion.length > 0 && <option key={'pro_sal_sup'} value="pro_sal_sup">Promedio salinidad-superficial</option>}
                      { state.data_estacion.length > 0 && <option key={'pro_tem_sup'} value="pro_tem_sup">Promedio temperatura-superficial</option>}
                      { state.data_estacion.length > 0 && <option key={'pro_tem_1'} value="pro_tem_1">Promedio temperatura - 1m</option>}
                      { state.data_estacion.length > 0 && <option key={'pro_tem_05'} value="pro_tem_05">Promedio temperatura - 0.5m</option>}
                      { state.data_estacion.length > 0 && <option key={'pro_ph_sub'} value="pro_ph_sub">Promedio ph-superficial</option>}
                      { state.data_estacion.length > 0 && <option key={'pro_ph_1'} value="pro_ph_1">Promedio ph-1m</option>}
                      { state.data_estacion.length > 0 && <option key={'1pro_ph_05'} value="pro_ph_05">Promedio ph-0.5m</option>}
                    </Form.Control>
                  </Form.Group>
                  <br />
                  <Button variant="outline-success" type="button" value="1" onClick={handleSubmit}>
                    Buscar
                  </Button>
                </Form>
        </>
    );
}

export default FFisicoQuimico;

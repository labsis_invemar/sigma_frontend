import React, { useEffect,useState,useReducer,useContext} from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import {url_departamento_ibim,url_estacion_ibim,url_datos_ibim} from '../api';
import {MapaContext} from '../../context/MapaContext'

const initialState = {
  data_departamento:[],
  data_estacion:[],
  data_info:[],

  id_estacion:null,
  id_departamento:null,
  id_info:null,
  id_estado:false,

  id_departamento_error:false,
  id_estacion_error:false,
}


// constantes para switch
const DEPARTAMENTO = "DEPARTAMENTO"
const ESTACION = "ESTACION"
const INFO = "INFO"
const LIMPIAR = "LIMPIAR"

const ID_ESTACION = "ID_ESTACION"
const ID_DEPARTAMENTO = "ID_DEPARTAMENTO"
const ID_INFO = "ID_INFO"
const ID_ESTADO = "ID_ESTADO"

const ID_DEPARTAMENTO_ERROR = "ID_DEPARTAMENTO_ERROR"
const ID_ESTACION_ERROR = "ID_ESTACION_ERROR"



const reducer = (state,action) => {
  
  switch (action.type) {
    case DEPARTAMENTO:
      return {...state,data_departamento:action.payload}

    case ESTACION:
      return {...state,data_estacion:action.payload}


    case INFO:
      return {...state,data_info:action.payload}
    
    case LIMPIAR:
      return {
        data_estacion:[],
        data_info:[],
      
        id_estacion:null,
        id_info:null,
        id_estado:false
      }

    
    case ID_DEPARTAMENTO:
      return {...state,id_departamento:action.payload , data_estacion : []} 
        
    case ID_ESTACION:
      return {...state,id_estacion:action.payload, data_parcela: []}

      
    case ID_INFO:
      return {...state,id_info:action.payload}
    
    case ID_ESTADO:
      return {...state,id_estado:action.payload}
    
    
      case ID_DEPARTAMENTO_ERROR:
      return {...state,id_departamento_error:action.payload}
    
    case ID_ESTACION_ERROR:
      return {...state,id_estacion_error:action.payload}
    
  
    default:
      return state;
  }

}



function FIndIBIm() {

  const [state, dispatch] = useReducer(reducer,initialState);
  const {SetEst}=useContext(MapaContext)
  const {setData}=useContext(MapaContext)
  const {data}=useContext(MapaContext)
  const {SetParcela}=useContext(MapaContext)
  const {SetmultipleParcelas}=useContext(MapaContext)


  //busca departamentos
  useEffect(()=>{
    fetch(url_departamento_ibim,{
      crossdomain: true,
    }).then(resp=>resp.json())
    .then(resp =>dispatch({type:DEPARTAMENTO,payload:resp}))
  },[state.id_departamento])



  //busca estacion
  useEffect(()=>{
    if(state.id_departamento){
       fetch(url_estacion_ibim(state.id_departamento),{
       crossdomain: true,
     }).then(resp=>resp.json())
     .then(resp =>dispatch({type:ESTACION,payload:resp}) )
    }
  },[state.id_departamento])


   //busca info 
   useEffect(()=>{
    if(state.id_info){
        fetch(url_datos_ibim(state.id_estacion),{
          crossdomain: true,
        }).then(res=>res.json())
        .then(res =>{
          setData(res);
          SetParcela(state.id_estacion);
          SetEst('Noibim');
          SetmultipleParcelas(true);
          dispatch({type:INFO,payload:res});
      }).then(dispatch({type:ID_ESTADO,payload:true}))
    }
  },[state.id_info])


  const handleSubmit = (()=>{
    if (state.id_departamento && state.id_estacion) {
      dispatch({type:ID_DEPARTAMENTO_ERROR,payload:false})
      dispatch({type:ID_ESTACION_ERROR,payload:false})
      dispatch({type:ID_INFO,payload:1})
    }
    else{
      if (!state.id_departamento) {
        dispatch({type:ID_DEPARTAMENTO_ERROR,payload:true})
        dispatch({type:ID_ESTACION_ERROR,payload:true})
      }
        else{
          if (!state.id_estacion){
            dispatch({type:ID_DEPARTAMENTO_ERROR,payload:false})
            dispatch({type:ID_ESTACION_ERROR,payload:true})
            
          }
        }
             
      
    }

  })

    return(
        <>
        <br />
                <Form>
                  <Form.Group controlId="exampleForm.ControlSelect1">
                    <Form.Label>Departamento</Form.Label>
                    <Form.Control as="select" onChange={(e)=> dispatch({type:ID_DEPARTAMENTO,payload:e.target.value})} defaultValue={'DEFAULT'} disabled={state.id_estado} isInvalid={state.id_departamento_error}>
                    <option value="DEFAULT">Opciones..</option>
                      {
                        state.data_departamento && state.data_departamento.map(dep => <option key={dep.id_departamento} value={dep.id_departamento} >{dep.departamento}</option>)
                      }
                    </Form.Control>
                  </Form.Group>
                  <Form.Group controlId="exampleForm.ControlSelect2">
                    <Form.Label>Estación</Form.Label>
                    <Form.Control as="select" onChange={(e)=> dispatch({type:ID_ESTACION,payload:e.target.value})} defaultValue={'DEFAULT'} disabled={state.id_estado} isInvalid={state.id_estacion_error}>
                    <option value="DEFAULT">Opciones..</option>
                      {
                      state.data_estacion && state.data_estacion.map(items => <option key={items.id_estacion} value ={items.id_estacion}> {items.estacion}</option>)
                      } 
                    </Form.Control>
                  </Form.Group>
                  <br />
                  <div className="btn-group mx-2" role="group" aria-label="primer boton">
                    <Button variant="outline-success" type="button" value="1" onClick={handleSubmit}>
                    Buscar
                    </Button>
                  </div>
                </Form>
        </>
    );
    
}

export default FIndIBIm;
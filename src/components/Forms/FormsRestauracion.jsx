import React, { useEffect, useState, useReducer, useContext } from "react";
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
// import Form from "react-bootstrap/Form";
import Card from "react-bootstrap/Card";
// import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";
import Tab from "react-bootstrap/Tab";
import Tabs from "react-bootstrap/Tabs";
import { MapaContext } from "../../context/MapaContext";
import FormsRestauracion from "./RestauracionForms/FormsConsultaRestauracion";
import GraficosRestauracion from '../Forms/RestauracionForms/GraficosRestauracion';
import {url_datos_siembra, url_departamento_estructura,url_siembraxdepartamento_restauracion,url_proyecto_restauracion_todos,url_areas_restauracion} from '../api';
// import GraficoRestauracion from "../Grafic/GraficoRestauracion";

const initialState = {
  data_siembra:[],
  data_siembraxdep:[],
  data_departamento:[],
  data_estacion:[],
  data_parcela:[],
  data_especie:[],
  data_variable:[],
  data_info:[],
  
  id_siembra:null,
  id_estacion:null,
  id_departamento:null,
  id_parcela:null,
  id_especie:null,
  id_variable:null,
  id_info:null,
  id_estado:false,

  id_departamento_error:false,
  id_estacion_error:false,
  id_parcela_error:false,
  id_especie_error:false,
  id_variable_error:false,
}

// constantes para switch
const SIEMBRA = "SIEMBRA"
const SIEMBRAXDEPARTAMENTO = "SIEMBRAXDEPARTAMENTO"
const DEPARTAMENTO = "DEPARTAMENTO"
const ESTACION = "ESTACION"
const PARCELA = "PARCELA"
const ESPECIE = "ESPECIE"
const VARIABLE = "VARIABLE"
const INFO = "INFO"
const LIMPIAR = "LIMPIAR"

const ID_SIEMBRA = "ID_SIEMBRA"

const ID_ESTACION = "ID_ESTACION"
const ID_DEPARTAMENTO = "ID_DEPARTAMENTO"
const ID_PARCELA = "ID_PARCELA"
const ID_ESPECIE = "ID_ESPECIE"
const ID_VARIABLE = "ID_VARIABLE"
const ID_INFO = "ID_INFO"
const ID_ESTADO = "ID_ESTADO"

const ID_DEPARTAMENTO_ERROR = "ID_DEPARTAMENTO_ERROR"
const ID_ESTACION_ERROR = "ID_ESTACION_ERROR"
const ID_PARCELA_ERROR = "ID_PARCELA_ERROR"
const ID_ESPECIE_ERROR = "ID_ESPECIE_ERROR"
const ID_VARIABLE_ERROR = "ID_VARIABLE_ERROR"

const reducer = (state,action) => {
  
  switch (action.type) {
    case SIEMBRA:
      return {...state,data_siembra:action.payload}

    case SIEMBRAXDEPARTAMENTO:
      return {...state,data_siembraxdep:action.payload}

    case DEPARTAMENTO:
      return {...state,data_departamento:action.payload}

    case ESTACION:
      return {...state,data_estacion:action.payload}

    case PARCELA:
      return {...state,data_parcela:action.payload}
    
    case ESPECIE:
      return {...state,data_especie:action.payload}
    
    case VARIABLE:
      return {...state,data_variable:action.payload}
    
    case INFO:
      return {...state,data_info:action.payload}
    
    case LIMPIAR:
      return {
        data_siembra:[],

        data_estacion:[],
        data_parcela:[],
        data_especie:[],
        data_variable:[],
        data_info:[],
      
        id_estacion:null,
        id_parcela:null,
        id_especie:null,
        id_variable:null,
        id_info:null,
        id_estado:false
      }

    case ID_SIEMBRA:
      return {...state,id_siembra:action.payload , data_siembra : []} 

    case ID_DEPARTAMENTO:
      return {...state,id_departamento:action.payload , data_estacion : []} 
        
    case ID_ESTACION:
      return {...state,id_estacion:action.payload, data_parcela: []}

    case ID_PARCELA:
      return {...state,id_parcela:action.payload, data_especie: []}
    
    case ID_ESPECIE:
      return {...state,id_especie:action.payload, data_variable: []}
    
    case ID_VARIABLE:
      return {...state,id_variable:action.payload}
      
    case ID_INFO:
      return {...state,id_info:action.payload}
    
    case ID_ESTADO:
      return {...state,id_estado:action.payload}
    
    
      case ID_DEPARTAMENTO_ERROR:
      return {...state,id_departamento_error:action.payload}
    
    case ID_ESTACION_ERROR:
      return {...state,id_estacion_error:action.payload}
    
    case ID_PARCELA_ERROR:
      return {...state,id_parcela_error:action.payload}
    
    case ID_ESPECIE_ERROR:
      return {...state,id_especie_error:action.payload}
    
    case ID_VARIABLE_ERROR:
      return {...state,id_variable_error:action.payload}
  
    default:
      return state;
  }

}

function FRestauracion() {
  const [state, dispatch] = useReducer(reducer,initialState);
  //const [total_siembra] = useReducer(reducer,initialState);
  const { SetCapaToribio } = useContext(MapaContext);
  const { EstRestauracion } = useContext(MapaContext);
  const [porcDep, setPorcDep] = useState([{departamento:'MAGDALENA',SIEMBRAXDEP:100}]);
  const [numProyectos, setNumProyectos] = useState(0);
  const [area, setArea] = useState(0);
  let Restauracion;
  
  useEffect(()=>{
    fetch(url_areas_restauracion,{
      crossdomain: true,
    }).then(resp=>resp.json())
    .then(resp =>{
      // console.log(resp);
      let array = [];
      resp.forEach(item => array.push(item.AREA));
      
      setArea(array.reduce((a1, a2) => a1 + a2));
  })
    
  },[state.id_siembra])
  //busca datos siembra
  useEffect(()=>{
    fetch(url_datos_siembra,{
      crossdomain: true,
    }).then(resp=>resp.json())
    .then(resp =>dispatch({type:SIEMBRA,payload:resp}))
    
  },[state.id_siembra])
 // console.log(state.data_siembra);
  var total_siembra = 0;
  state.data_siembra.map((item)=>{return total_siembra+=item.cant_arb})

//buscamos la cantidad de proyectos
useEffect(()=>{
  fetch(url_proyecto_restauracion_todos,{
    crossdomain: true,
  }).then(resp=>resp.json())
    .then(data =>setNumProyectos([data].length+1))
    
},[numProyectos])
// console.log(numProyectos)

//busca departamentos
useEffect(()=>{
  fetch(url_departamento_estructura,{
    crossdomain: true,
  }).then(resp=>resp.json())
  .then(resp =>dispatch({type:DEPARTAMENTO,payload:resp}))
},[state.id_departamento])
// console.log(JSON.stringify(state.data_departamento))

//busca el porcentaje de siembra por departamento
useEffect(()=>{
 fetch(url_siembraxdepartamento_restauracion,{
   crossdomain: true,
 }).then(resp=>resp.json())
   //.then(data =>console.log(data))
   .then(data =>setPorcDep(data))
   //.then(resp =>console.log(resp))
},[]);
//  console.log(porcDep);

  const EstadoToribio = () => {
    const checkToribio = document.getElementById("check1");
    if (checkToribio.checked) {
      SetCapaToribio(true);
    } else {
      SetCapaToribio(false);
    }
  };

  if (EstRestauracion === true) {
    // Restauracion = <GrafRestauracion/>
    Restauracion = <GraficosRestauracion/>
    // Restauracion = <GraficoRestauracion/>
  }else{
    Restauracion = <FormsRestauracion/>

  }





  const options = {
    chart: {
      type: "pie",
    },
    title: {
      // text: "Hectareas en procesos de restauración por departamento",
      text: "Hectareas en procesos de restauración por departamento",
    },
    xAxis: {
      title: {
        text: "Valores",
        align: "high",
      },
      labels: {
        overflow: "justify",
      },
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: "pointer",
        dataLabels: {
          enabled: false,
        },
        showInLegend: true,
      },
    },
    series: [
      {
        name: "% de siembra",
        colorByPoint: true,
        data: porcDep.map((dep) =>{
          return {name: dep.departamento,y:dep.porc_siembraxdep}
        })
        
      },
    ],
  };

  return (
    <>
      <Tabs defaultActiveKey="contexto" id="uncontrolled-tab-example">
        <Tab eventKey="contexto" title="Contexto">
          <div id="StyleTabsColor">
          {/* <h6>{numProyectos}</h6> */}
          {/* {console.log(JSON.stringify(state.datos_siembra))} */}
            <p align="justify" id="textToribio">
              La restauración ecológica en Colombia desde inicios de la década
              de los noventas fue promovida por instituciones no gubernamentales
              y universidades, desde una perspectiva principalmente académica y
              teórica. Posteriormente, los primeros pilotos fueron ejecutados en
              ecosistemas continentales como bosque alto Andino, entre otros.
              Paralelamente, la práctica de la restauración ecológica ha tomado
              relevancia legislativa a escala nacional, puesto que desde el
              Ministerio de Medio Ambiente y Desarrollo-MADS se ha promovido la
              generación de herramientas legislativas y normativas que promuevan
              el desarrollo de programas de restauración ecológica participativa
              para entidades de carácter público y/o privado cuyas actividades
              impliquen degradación y/o pérdida del capital natural, siguiendo
              los lineamientos del Plan Nacional de Restauración como hoja de
              ruta para dichos procesos. <br /> Dentro de los ecosistemas
              estratégicos identificados con necesidad de restauración en el
              país se destacan los marino-costeros, en especial los ecosistemas
              de manglar debido al sinnúmero de bienes y servicios que estos
              prestan a las comunidades locales ubicadas en estas zonas del
              país, esto aunado al avanzado deterioro que estos ecosistemas han
              sufrido en la última década.
            </p>
            <Button
              variant="success"
              href="http://sigma.invemar.org.co/restauracion"
              target="_blank"
            >
              Ver más.
            </Button>
          </div>
        </Tab>
        <Tab eventKey="info" title="Información general">
          <div id="StyleTabsColor StyleScroll">
            
            <div className="row">
            <h4>

            {/* <br /><b>Cifras de restauracion de manglar</b> */}
            <br/></h4>
              <div className="col-sm-8 mx-auto text-center">
                <Card bg="success" style={{ padding:"0.5rem" }} text="white" >
                  <h6>
                  Area reportadas en proceso de restauración
                  </h6>
                  <h3><b>{area.toFixed(4).toString().replace(/\./g, ',')} ha</b></h3>
                </Card>
              </div>
             
                         
            </div>
            <br />
            <div className="row">
              <div className="col-sm-12 col-md-6 text-center">
                <Card bg="success" style={{ width: "14rem", paddingTop: "0.4rem", paddingBottom: "0.4rem"}} text="white">
                  <h6>No. de arboles sembrados</h6>
                  <h3><b>{total_siembra}</b></h3>
                </Card>
              </div>

              <div className="col-sm-12 col-md-6 text-center">
                <Card bg="success" style={{ width: "14rem", paddingTop: "0.4rem", paddingBottom: "0.4rem" }} text="white">
                  <h6>No. de proyectos registrados</h6>
                  <h3><b>{numProyectos}</b></h3>
                </Card>
              </div>
            </div>
            <br />
            <div className="container-fluid">
              <HighchartsReact highcharts={Highcharts} options={options} />
            </div>
          </div>
        </Tab>
        <Tab eventKey="consulta" title="Consulta">
          <div id="StyleTabsColor StyleScroll">
            
            {Restauracion}
          </div>
        </Tab>
        
      </Tabs>
      
      
    </>
  );
}

export default FRestauracion;

import React,{useContext} from "react";
import Form from "react-bootstrap/Form";
import {MapaContext} from '../../context/MapaContext'

function FSeriesCispata({handle}) {
  const {SetSerieTiempo}=useContext(MapaContext)


  const presione=(e)=>{
    if (e.target.checked === true) {
      SetSerieTiempo(true)
    }else{
      SetSerieTiempo(false)
    }
  }

  return (
    <>
    <br/>
    <br />
      <p align="left">
        Este módulo permite la visualización de la series de tiempo de cispata registradas en
        SIGMA. 
      </p>
      <Form>
        {["checkbox"].map((type) => (
          <div key={`default`} className="mb-3">
            <Form.Check type={type} id={"Check1"} label={"Activar visualización"} onClick={presione} />
          </div>
        ))}
      </Form>
    
    </>
  );
}

export default FSeriesCispata;
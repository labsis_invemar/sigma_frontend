import React, { useEffect, useReducer, useContext } from "react";
import { MapaContext } from "../../../context/MapaContext";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import {
        url_municipio_restauracion,
        url_proyecto_restauracion,
        url_departamento_restauracion,
        url_estacion_restauracion,
        url_anios_restauracion,
        url_data_general_restauracion,
      } from "../../api";

const initialState = {
  data_departamento:[],
  data_municipio:[],
  data_proyecto:[],
  data_estacion:[],
  data_parcela:[],
  data_especie:[],
  data_variable:[],
  data_info:[],
  data_anios:[],

  id_proyecto:null,
  id_estacion:null,
  cod_departamento:null,
  id_municipio:null,
  id_parcela:null,
  id_especie:null,
  id_variable:null,
  id_info:null,
  id_anio:null,
  id_estado:false,

  id_departamento_error:false,
  id_municipio_error:false,
  id_proyecto_error:false,
  id_estacion_error:false,
  id_parcela_error:false,
  id_especie_error:false,
  id_variable_error:false,
  id_anio_error:false,
}


// constantes para switch
const DEPARTAMENTO = "DEPARTAMENTO"
const MUNICIPIO = "MUNICIPIO"
const PROYECTO = "PROYECTO"
const ESTACION = "ESTACION"
const PARCELA = "PARCELA"
const ESPECIE = "ESPECIE"
const VARIABLE = "VARIABLE"
const ANIO = "ANIO"
const INFO = "INFO"
const LIMPIAR = "LIMPIAR"


const COD_DEPARTAMENTO = "ID_DEPARTAMENTO"
const ID_MUNICIPIO = "ID_MUNICIPIO"
const ID_PROYECTO = "ID_PROYECTO"
const ID_ESTACION = "ID_ESTACION"
const ID_PARCELA = "ID_PARCELA"
const ID_ESPECIE = "ID_ESPECIE"
const ID_ANIO = "ID_ANIO"
const ID_INFO = "ID_INFO"
const ID_ESTADO = "ID_ESTADO"
// const ID_ESPECIE = "ID_ESPECIE"

const ID_DEPARTAMENTO_ERROR = "ID_DEPARTAMENTO_ERROR"
const ID_MUNICIPIO_ERROR = "ID_MUNICIPIO_ERROR"
const ID_PROYECTO_ERROR = "ID_PROYECTO_ERROR"
const ID_ESTACION_ERROR = "ID_ESTACION_ERROR"
const ID_PARCELA_ERROR = "ID_PARCELA_ERROR"
const ID_ESPECIE_ERROR = "ID_ESPECIE_ERROR"
const ID_ANIO_ERROR = "ID_ANIO_ERROR"
const ID_VARIABLE_ERROR = "ID_VARIABLE_ERROR" 

const reducer = (state,action) => {
  
  switch (action.type) {
    case DEPARTAMENTO:
      return {...state,data_departamento:action.payload}

    case MUNICIPIO:
      return {...state,data_municipio:action.payload}
      
    case PROYECTO:
      return {...state,data_proyecto:action.payload}

    case ESTACION:
      return {...state,data_estacion:action.payload}

    case PARCELA:
      return {...state,data_parcela:action.payload}
    
    case ESPECIE:
      return {...state,data_especie:action.payload}
    
    case ANIO:
      return {...state,data_anios:action.payload}

    case VARIABLE:
      return {...state,data_variable:action.payload}

    case INFO:
      return {...state,data_info:action.payload}
    
    case LIMPIAR:
      return {
        data_departamento:[],
        data_municipio:[],
        data_proyecto:[],
        data_estacion:[],
        data_parcela:[],
        data_especie:[],
        data_variable:[],
        data_info:[],
        data_anios:[],

        cod_departamento:null,	
        id_municipio:null,
        id_proyecto:null,
        id_estacion:null,
        id_parcela:null,
        id_especie:null,
        id_variable:null,
        id_anio:null,
        id_info:null,
        id_estado:false,
      }

    
    case COD_DEPARTAMENTO:
      return {...state,cod_departamento:action.payload , data_proyecto : []} 
   
    case ID_MUNICIPIO:
      return {...state,id_municipio:action.payload , data_proyecto : []} 
      
    case ID_PROYECTO:
      return {...state,id_proyecto:action.payload , data_estacion : []} 

    case ID_ESTACION:
      return {...state,id_estacion:action.payload, data_parcela: []}

    case ID_PARCELA:
      return {...state,id_parcela:action.payload}
    
    case ID_ESPECIE:
      return {...state,id_especie:action.payload, data_variable: []}
    
    case ID_ANIO:
        return {...state,id_anio:action.payload}
      
    case ID_INFO:
      return {...state,id_info:action.payload}
    
    case ID_ESTADO:
      return {...state,id_estado:action.payload}
    
    

    case ID_PROYECTO_ERROR:
      return {...state,id_proyecto_error:action.payload}

    case ID_DEPARTAMENTO_ERROR:
      return {...state,id_departamento_error:action.payload}

    case ID_MUNICIPIO_ERROR:
      return {...state,id_municipio_error:action.payload}

    case ID_ESTACION_ERROR:
      return {...state,id_estacion_error:action.payload}
    
    case ID_PARCELA_ERROR:
      return {...state,id_parcela_error:action.payload}
    
    case ID_ESPECIE_ERROR:
      return {...state,id_especie_error:action.payload}   

    case ID_ANIO_ERROR:
      return {...state,id_anio_error:action.payload}
    default:
      return state;
  }

}



function FormsRestauracion() {
  const { SetEstRestauracion } = useContext(MapaContext);
  const [state, dispatch] = useReducer(reducer,initialState);
  const {setData}=useContext(MapaContext)
  const {est, SetEst}=useContext(MapaContext)
  const {data}=useContext(MapaContext)
  const {SetParcela}=useContext(MapaContext)
  const {SetEstadoMapa}=useContext(MapaContext)
  //const { EstRestauracion } = useContext(MapaContext);
  // const Consulta = () => {
  //   SetEstRestauracion(true);
  // }

  useEffect(() => {
    if (data === LIMPIAR) {
      SetEst('Noest');
      dispatch({type:LIMPIAR})
    }
  }, [data])
  


//busca departamentos
  useEffect(()=>{
    
      fetch(url_departamento_restauracion,{
        crossdomain: true,
      }).then(resp=>resp.json())
      .then(resp =>{
            dispatch({type:DEPARTAMENTO,payload:resp})
            if (resp.length ===1) {
              console.log(resp);
              dispatch({type:COD_DEPARTAMENTO,payload:resp[0].cod_departamento})
            }
          }
          )          
  },[])
  

//busca municipio
useEffect(()=>{
  if(state.cod_departamento){
    console.log('hay dep hago el fetch')
     fetch(url_municipio_restauracion(state.cod_departamento),{
     crossdomain: true,
   }).then(respm=>respm.json())
   
   .then(respm => {
     console.log('resp.length',respm.length);
     console.log('respm',respm);
     dispatch({ type: MUNICIPIO, payload: respm });
      //  if (respm.length === 1) {
      //   console.log('voy aqui');
      //    dispatch({ type: ID_MUNICIPIO, payload: respm[0].id_municipio });
      //  }
     }

   )
}
},[state.cod_departamento])
  
//busca proyecto
  useEffect(()=>{
    
       if(state.id_municipio){
         
          fetch(url_proyecto_restauracion(state.id_municipio),{
          crossdomain: true,
        }).then(resp=>resp.json())
        .then((resp) => {
          dispatch({ type: PROYECTO, payload: resp });
            if (resp.length === 1) {
              
              dispatch({ type: ID_PROYECTO, payload: resp[0].id_proyecto });
            }
          }
        )
    }
  },[state.id_municipio])

  //busca estacion 
  useEffect(()=>{
    
    if(state.id_proyecto && state.cod_departamento){
        fetch(url_estacion_restauracion(state.id_municipio,state.id_proyecto),{
          crossdomain: true,
        }).then(res=>res.json())
        .then(res =>{dispatch({type:ESTACION,payload:res})
        if (res.length === 1) {
          dispatch({ type: ID_ESTACION, payload: res[0].id_estacion });
        }
        }  
      )
    }
  },[state.id_proyecto])

  //busca los años 
  useEffect(()=>{
    if(state.id_estacion){
    
        fetch(url_anios_restauracion(state.id_estacion,state.id_proyecto),{
          crossdomain: true,
          
        }).then(res=>res.json())
        .then(res =>{dispatch({type:ANIO,payload:res})
        
        
        if (res.length === 1) {
          dispatch({ type: ID_ANIO, payload: res[0].ano });
        }
        } )
    }
  },[state.id_estacion])
 

    //busca info 
    useEffect(()=>{
      if(state.id_info){
         console.log(`-id_anio:${state.id_anio}-id_proj: ${state.id_proyecto}-id_parcela:${state.id_estacion}`);
            fetch(url_data_general_restauracion(state.id_anio,state.id_proyecto,state.id_estacion),{
            crossdomain: true,
          }).then(res=>res.json())
          .then(res =>{
            setData(res);
            SetParcela(state.id_estacion);
            SetEst('Nores');
            SetEstRestauracion(true)
            SetEstadoMapa(true);
            dispatch({type:INFO,payload:res});
        }).then(dispatch({type:ID_ESTADO,payload:true}))
       }
    },[state.id_info])
    //console.log(data)
    //Función para verificar los campos y consultar info
  const handleSubmit = (()=>{
    if (state.id_municipio && state.id_estacion ) {
      dispatch({type:ID_DEPARTAMENTO_ERROR,payload:false})
      dispatch({type:ID_MUNICIPIO_ERROR,payload:false})
      dispatch({type:ID_ESTACION_ERROR,payload:false})
      //dispatch({type:ID_PARCELA_ERROR,payload:false})
     // dispatch({type:ID_ESPECIE_ERROR,payload:false})
      //dispatch({type:ID_VARIABLE_ERROR,payload:false})
      dispatch({type:ID_INFO,payload:1})
    }
    else{
      if (!state.cod_departamento) {
        dispatch({type:ID_DEPARTAMENTO_ERROR,payload:true})
        dispatch({type:ID_MUNICIPIO_ERROR,payload:false})
        dispatch({type:ID_ESTACION_ERROR,payload:true})
        dispatch({type:ID_PARCELA_ERROR,payload:true})
        dispatch({type:ID_ESPECIE_ERROR,payload:true})
        dispatch({type:ID_VARIABLE_ERROR,payload:true})
      }
        else
          if (!state.id_estacion){
            dispatch({type:ID_DEPARTAMENTO_ERROR,payload:false})
            dispatch({type:ID_ESTACION_ERROR,payload:true})
            dispatch({type:ID_PARCELA_ERROR,payload:true})
            dispatch({type:ID_ESPECIE_ERROR,payload:true})
            dispatch({type:ID_VARIABLE_ERROR,payload:true})
          }
          // else
          //   if (!state.id_parcela){
          //     dispatch({type:ID_DEPARTAMENTO_ERROR,payload:false})
          //     dispatch({type:ID_ESTACION_ERROR,payload:false})
          //     dispatch({type:ID_PARCELA_ERROR,payload:true})
          //     dispatch({type:ID_ESPECIE_ERROR,payload:true})
          //     dispatch({type:ID_VARIABLE_ERROR,payload:true})
          //   }
            else
              if (!state.id_especie){
                dispatch({type:ID_DEPARTAMENTO_ERROR,payload:false})
                dispatch({type:ID_ESTACION_ERROR,payload:false})
                dispatch({type:ID_PARCELA_ERROR,payload:false})
                dispatch({type:ID_ESPECIE_ERROR,payload:true})
                dispatch({type:ID_VARIABLE_ERROR,payload:true})
              }
            //   else
            //     if (!state.id_variable) {
            //       dispatch({type:ID_DEPARTAMENTO_ERROR,payload:false})
            //       dispatch({type:ID_ESTACION_ERROR,payload:false})
            //       dispatch({type:ID_PARCELA_ERROR,payload:false})
            //       dispatch({type:ID_ESPECIE_ERROR,payload:false})
            //       dispatch({type:ID_VARIABLE_ERROR,payload:true})
            //     }
      
    }

  })

  return (
    <>
     <div className="container-fluid">
                <h5><b><br/>Indicadores para la restauraci&oacute;n de manglar</b></h5>
              </div>
      <Form>

      <Form.Group controlId="exampleForm.ControlSelect1">
          <Form.Label>Departamento</Form.Label>
          <Form.Control as="select" onChange={(e)=> dispatch({type:COD_DEPARTAMENTO,payload:e.target.value})} value={state.cod_departamento? state.cod_departamento: 'DEFAULT'} disabled={state.id_estado} isInvalid={state.id_departamento_error}>
            <option value="DEFAULT"  >Opciones..</option>
            {
             state.data_departamento && state.data_departamento.map(dep => <option key={dep.cod_departamento} value={dep.cod_departamento} >{dep.departamento}</option>) 
            }
          </Form.Control>
        </Form.Group>

      <Form.Group controlId="exampleForm.ControlSelect2">
        <Form.Label>Municipio</Form.Label>
        <Form.Control as="select" onChange={(e)=> dispatch({type:ID_MUNICIPIO,payload:e.target.value})} value = {state.id_municipio ? state.id_municipio : 'DEFAULT'} disabled={state.id_estado} isInvalid={state.id_municipio_error}>
          <option value="DEFAULT">Opciones..</option>
          {
            state.data_municipio && state.data_municipio.map(mun => <option key={mun.id_municipio}  value={mun.id_municipio} selected={mun.id_municipio === state.id_municipio} >{mun.municipio}</option>)
          }
        </Form.Control>
      </Form.Group>

      <Form.Group controlId="exampleForm.ControlSelect3">
          <Form.Label>Proyecto</Form.Label>
          <Form.Control as="select" onChange={(e)=> dispatch({type:ID_PROYECTO,payload:e.target.value})} value={state.id_proyecto? state.id_proyecto: 'DEFAULT'} disabled={state.id_estado} isInvalid={state.id_proyecto_error}>
            <option value="DEFAULT">Opciones..</option>
            {
             state.data_proyecto && state.data_proyecto.map(pry => <option key={pry.id_proyecto}  value={pry.id_proyecto} selected={pry.id_proyecto === state.id_proyecto} >{pry.proyecto}</option>)
            }
          </Form.Control>
        </Form.Group>
       
        <Form.Group controlId="exampleForm.ControlSelect4">
          <Form.Label>Estación</Form.Label>
          <Form.Control as="select" onChange={(e)=> dispatch({type:ID_ESTACION,payload:e.target.value})} value={state.id_estacion? state.id_estacion: 'DEFAULT'} disabled={state.id_estado} isInvalid={state.id_estacion_error}>
          <option value="DEFAULT">Opciones..</option>
          
            { 
             state.data_estacion && state.data_estacion.map(pry => <option key={pry.id_estacion} value={pry.id_estacion} selected={pry.id_estacion === state.id_estacion}  >{pry.estacion}</option>)
            }
          </Form.Control>
        </Form.Group>
        
        <Form.Group controlId="exampleForm.ControlSelect5">
          <Form.Label>Año</Form.Label>
          <Form.Control as="select" onChange={(e)=> dispatch({type:ID_ANIO,payload:e.target.value})} value={state.id_anio? state.id_anio: 'DEFAULT'} disabled={state.id_estado} isInvalid={state.id_anio_error}>
          <option value="DEFAULT">Opciones..</option>
          
            {
              state.data_anios && state.data_anios.map(a => <option key={a.ano} value={a.ano} selected={a.ano === state.id_anio} >{a.ano}</option>)
            }
          </Form.Control>
        </Form.Group>
      
        <br />
          
        <div className="btn-group mx-2" role="group" aria-label="primer boton">
            <Button variant="outline-success" type="button" value="1" onClick={handleSubmit}>
              Buscar
            </Button>
          </div>
      </Form>
    </>
  );
}

export default FormsRestauracion;

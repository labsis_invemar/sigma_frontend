import React from "react";
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";

let options = {};

function GrafPrototipo({titulo,tipo, data, text_x='', text_y='text y'}) {
  
  if(tipo === 1){//grafico de torta
    options = {
      chart: {
        type: "pie",
      },
      title: {
        text: titulo,
      },
      xAxis: {
        title: {
          text: text_x,
          align: "high",
        },
        labels: {
          overflow: "justify",
        },
      },
      
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: "pointer",
          dataLabels: {
            enabled: false,
          },
          showInLegend: true,
        },
      },
      series: 
      [
        {
          name: "Densidad (ind/mt²)",
          colorByPoint: true,

          data: data, 
        
        },
      ],
    };
  }else{ //grafico de barras
      /*---Procedimiento para preparar datos para grafica---*/
      let fechas = [];
      let especies = {};
      let series=[]
    if(data != null) {
    console.log(data)
    const key = Object.keys(data[0])[2];
    
    console.log("key",key)
    console.log("data",data);
    data.forEach((dato) => {
      if (!fechas.includes(dato.fecha)) {
        fechas.push(dato.fecha);
      }

      if (dato.especie in especies) {
        especies[dato.especie].push(dato[key]);
      } else {
        especies[dato.especie] = [dato[key]];
      }
    });
    Object.keys(especies).forEach(esp=>{
      series.push({
          "name":esp,
          "data":especies[esp]
      })
    })
    
  // console.log("series",series)
  // console.log("fechas",fechas)
  

  }
    

    options = {
      chart: {
        type: "column",
      },
      title: {
        text: titulo,
      },

    xAxis: {
      title: {
        text: "Año",
        align: "high",
      },
      categories: fechas /* Aqui va los Años */,
      labels: {
        overflow: "justify",
      },
    },

    yAxis: {
        title: {
            text: text_y
        },
 
       
      },
      series:series,
 
    };

    

  }
    
    return (
        <>
            <div className="container-fluid grafRestauracion">
            <HighchartsReact highcharts={Highcharts} options={options} />
            </div>
        </>
    );
};

export default GrafPrototipo;
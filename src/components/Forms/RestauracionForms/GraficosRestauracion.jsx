import React, { useEffect, useState, useContext } from "react";
import { MapaContext } from "../../../context/MapaContext";
import Table from "react-bootstrap/Table";
// import Highcharts from "highcharts";
// import HighchartsReact from "highcharts-react-official";
import Button from "react-bootstrap/Button";
import Grafprototipo from "./GraficasPrototipo";
import { url_datos_restauracion_estacion, url_dens_prop_restauracion, url_dens_plant_restauracion, url_supervivencia_estacion, url_mortalidad_estacion } from "../../api";


const titulo1 = "Densidad de las plantulas"
const titulo2 = "Densidad de propagulos"
const titulo3 = "Tasa de mortalidad"
const titulo4 = "Tasa de supervivencia"
const titulo5 = "Cantidad de individuos sembrados por especie"


function GraficasRestauracion() {
  const { SetEstRestauracion } = useContext(MapaContext);
  const {data}=useContext(MapaContext);
  const [siembra, setSiembra] = useState([]);
  const [mortalidad, setMortalidad] = useState([]);
  const [supervivencia, setSupervivencia] = useState([]);
  const [densprop, setDensprop] = useState([]);
  const [densplant, setDensplant] = useState([]);
  
  
  const handleClose = () => {
    SetEstRestauracion(false);
  };
  
 
  //siembra por especie
  useEffect(() => {

    if(data){
      // console.log(data[0].ano,data[0].id_estacion);
      fetch(url_datos_restauracion_estacion(data[0].ano,data[0].id_estacion),{
        crossdomain: true
      }).then(response=>response.json())
      .then(response=>{
        
         const respuesta = response.map(mata=>{
            const {especie, siembraxesp} = mata;
            const sembraditos = {name: especie, y:siembraxesp};
            return (sembraditos);
          })
          console.log('siembr:',respuesta);


          setSiembra(respuesta)
        
        })
        // .then(response=>response.map(dato=>(
        //     console.log(dato)
        // )))

        fetch(url_dens_prop_restauracion(data[0].id_estacion,data[0].ano),{
          crossdomain: true
        }).then(response=>response.json())
        .then(response=>{
          const respuesta = response.map(denspr=>{
            const {especie, dens_propagulos} = denspr;
            const densisdadpr = {name: especie, y:dens_propagulos};
            return (densisdadpr);
          })
          console.log('prop:',respuesta);
          setDensprop(respuesta)
        })

        fetch(url_dens_plant_restauracion(data[0].id_estacion,data[0].ano),{
          crossdomain: true
        }).then(response=>response.json())
        .then(response=>{
          const respuesta = response.map(denspl=>{
            const {especie, dens_plantulas} = denspl;
            const densisdadpl = {name: especie, y:dens_plantulas};
            return (densisdadpl);
          })
          
          console.log('plant:',respuesta);
          setDensplant(respuesta)
        })


        if(data[0].N_MONITOREOS>0){
         
          fetch(url_supervivencia_estacion(data[0].id_estacion),{
            crossdomain: true
          }).then(response=>response.json())
          .then(response=>{
            // console.log(response);
            const respuesta = response.map(vivos=>{
              console.log(vivos);
              
              const {FECHA, ESPECIE_DES,TASA_SUPERVIVENCIA} = vivos;
              const sobrevivientes = {fecha: FECHA, especie: ESPECIE_DES, TS: TASA_SUPERVIVENCIA};
              return (sobrevivientes);
            })
            console.log('supervivencia:',respuesta);
            
            
            setSupervivencia(respuesta)
          })

          fetch(url_mortalidad_estacion(data[0].id_estacion),{
            crossdomain: true
          }).then(response=>response.json())
          .then(response=>{
            // console.log(response);
            const respuesta = response.map(muertos=>{
              console.log(muertos);
            
                const {FECHA, ESPECIE_DES,TASA_MORTALIDAD} = muertos;
                const fallecidos = {fecha: FECHA, especie: ESPECIE_DES, TM: TASA_MORTALIDAD};
                return (fallecidos);
            })
            console.log('mortalidad:',respuesta);
            setMortalidad(respuesta)
          })
        }
        

      }
      

    },[data])
    
    
  

  return (
    <>
      <div id="StyleTabsColor">
        <div className="container-fluid" id="StyleScroll">
          <div className="container-fluid">
            <a id="StyleA">
              Información
              <Button
                variant="close"
                id="buttonClose"
                onClick={handleClose}
              ></Button>
            </a>
          </div>
          <div className="table-responsive tableRestauracion">
            <Table className="table animate__animated animate__fadeIn" striped bordered hover>
              <thead>
                <tr>
                  <th>Items</th>
                  <th>Info</th>
                </tr>
              </thead>
              <tbody>
              
                <tr>
                  <td>Proyecto</td>
                  <td>{data[0].proyecto}</td>
                    {/* <td>{console.log(datos)}</td> */}
                </tr>
                <tr>
                  <td>Estacion</td>
                  <td>{data[0].estacion}</td>
                </tr>
                <tr>
                  <td>Año</td>
                  <td>{data[0].ano}</td>
                </tr>
                {/* <tr>
                  <td>Alcance del proyecto</td>
                  <td>***2 hectareas</td>
                </tr>
                <tr>
                  <td>Objetivos de restauración</td>
                  <td>***2221 arboles</td>
                </tr> */}
                <tr>
                  <td>Ha en proceso de restauración</td>
                  {/* <td>{data[0].ESPXEST}</td> */}
                  <td>{data[0].AREA}</td>
                </tr>
                <tr>
                  <td>Entidad Financiadora</td>
                  <td>{data[0].ENTIDAD_FINANCIADORA}</td>
                </tr> 
                <tr>
                  <td>Entidad implementadora</td>
                  <td>Invemar</td>
                </tr>
                <tr>
                  <td>Fuente de financiaci&oacute;n</td>
                  <td>{data[0].FUENTE_FINANCIACION}</td>
                </tr>
                <tr>
                  <td>No de árboles sembrados</td>
                  <td>{data[0].siembraxest}</td>
                </tr>
                <tr>
                  <td>Especies sembradas</td>
                  <td>{data[0].ESPXEST}</td>
                </tr>
                <tr>
                  <td>Fecha inicio de monitoreo</td>
                  <td>{data[0].FECHA_INICIAL_MONITOREOS}</td> 
                </tr>
                <tr>
                  <td>Numero de monitoreos realizados</td>
                  <td>{data[0].N_MONITOREOS}</td> 
                </tr>
              </tbody>
            </Table>
          </div>
          <br />

          <Grafprototipo titulo={titulo5} tipo={1} data={siembra} /><br/>

          {densplant.some(value=>value.y !== 0) ?<Grafprototipo titulo={titulo1} tipo={1} data={densplant} />:
          <div className="card">
           <div className="card-body">
            <h5>{titulo1}</h5>
            <h6>Informacion no disponible</h6>
            </div>
          </div>
          }
          
          
          {densprop.some(value=>value.y !== 0) ?<Grafprototipo titulo={titulo2} tipo={1} data={densprop} />:
          <div className="card">
           <div className="card-body">
            <h5>{titulo2}</h5>
            <h6>Informacion no disponible</h6>
            </div>
          </div>
          }
          {mortalidad.length>0 ? <Grafprototipo titulo={titulo3} tipo={2} data={mortalidad} text_y={'porcentaje de muertes'}/>:
          <div className="card">
           <div className="card-body">
            <h5>{titulo3}</h5>
            <h6>Informacion no disponible</h6>
            </div>
          </div>
          }
          {supervivencia.length>0 ?<Grafprototipo titulo={titulo4} tipo={2} data={supervivencia} text_y={'porcentaje de sobrevivientes'}/>:
          <div className="card">
           <div className="card-body">
            <h5>{titulo4}</h5>
            <h6>Informacion no disponible</h6>
            </div>
          </div>}

        </div>
      </div>
    </>
  );
}

export default GraficasRestauracion;

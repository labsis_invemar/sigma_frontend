// import React, { useContext, useEffect, useState } from "react";
import React, { useContext } from "react";
import Button from "react-bootstrap/Button";
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import { MapaContext } from "../../context/MapaContext";
import GraficosTabla from "./GraficosTabla";
// import { isFunction } from "formik";

function GrafEstruc(props) {
  const { SetEst } = useContext(MapaContext);
  // const { setData } = useContext(MapaContext);
  const { data } = useContext(MapaContext);
  const { SetEstadoMapa } = useContext(MapaContext);

/*---Procedimiento para preparar datos para grafica---*/
  let anios = [];
  let especies = {};
  const key = Object.keys(data[0])[2];
  
  // console.log("key",key)
  let series=[]
  // console.log("data",data);
  data.forEach((dato) => {
    if (!anios.includes(dato.ano)) {
      anios.push(dato.ano);
      
    }

    if (dato.especie in especies) {
      especies[dato.especie].push(dato[key]);
    } else {
      especies[dato.especie] = [dato[key]];
    }
  });
  Object.keys(especies).forEach(esp=>{
    series.push({
        "name":esp,
        "data":especies[esp]
    })
  })
  
//console.log("series",series)
console.log("años",anios)
/*-------------------------------------*/


  /* ---------------------------------- */

  const handleCl = () => {
    SetEst('Noest');
    SetEstadoMapa(false);
    anios=[];
    especies=[];
    series=[];
  };


  const options = {
    chart: {
      type: "column",
    },
    title: {
      text: key.toUpperCase().replace("_", " ").replace("_", " ")//.replace("DENS", "DENSIDAD").replace("PLANT", "PLANTULAS").replace("PROP", "PROPAGULOS").replace("X", " POR").replace("ESP", " ESPECIE")//.concat(' EN "',parcela,'"'),
    },
    xAxis: {
      title: {
        text: "Año",
        align: "high",
      },
      categories: anios /* Aqui va los Años */,
      labels: {
        overflow: "justify",
      },
    },
    yAxis: {
      title: {
        text: "Valor",
      },
    },
    series:series,
  };
  return (
    <>
      <div className="container-fluid" id="StyleGraficoEstructura">
        <div className="container-fluid">
          <HighchartsReact highcharts={Highcharts} options={options} />
        </div>
        <br />
        <div className="table-responsive" >
          <GraficosTabla/>
          {/* <Table className="table table-dark" striped bordered hover>
            <thead>
              <tr>
                <th>Año</th>
                <th>Especie</th>
                <th>Valor</th>
                <th>Unidad de medida</th>
              </tr>
            </thead>
            <tbody>
                {data.map(dato=>
                <tr>
                    <td>{dato.ano}</td>
                    <td>{dato.especie}</td>
                    <td>{dato[key]}</td>
                    <td>{"ind/ha"}</td>

                </tr>
                    )}

            </tbody>
              
          </Table> */}
        </div>
        <br />
        <div className="btn-group mx-2" role="group" aria-label="segundo boton">
          <Button
            variant="outline-primary"
            type="button"
            value="1"
            onClick={handleCl}
          >
            Limpiar
          </Button>
        </div>
      </div>
    </>
  );
}

export default GrafEstruc;

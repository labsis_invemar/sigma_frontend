import React, { useContext, useEffect, useState } from "react";
import Button from "react-bootstrap/Button";
import Table from "react-bootstrap/Table";
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import { MapaContext } from "../../context/MapaContext";
import { isFunction } from "formik";

function GrafIbim(props) {
  const { SetEst } = useContext(MapaContext);
  const { setData } = useContext(MapaContext);
  const { data } = useContext(MapaContext);
  const { SetmultipleParcelas } = useContext(MapaContext);

/*---Procedimiento para preparar datos para grafica---*/
  let anios = [];
  let informacion = {};
  const key = Object.keys(data[0])[1];
  let series=[]

  console.log(data)
  data.forEach((dato) => {
    if (!anios.includes(dato.ano)) {
      anios.push(dato.ano);
      
    }

    if (dato.calificacion in informacion) {
      informacion[dato.calificacion].push(dato[key]);
    } else {
      informacion[dato.calificacion] = [dato[key]];
    }
    console.log(dato[key])
  });
  Object.keys(informacion).forEach(esp=>{
    series.push({
        "name":esp,
        "data":informacion[esp]
    })
  })
  
console.log(series)
/*-------------------------------------*/


  /* ---------------------------------- */

  const handleClick = () => {
    SetmultipleParcelas(false);
    SetEst('ibim');
    anios=[];
    informacion=[];
    series=[];
  };


  const options = {
    chart: {
      type: "column",
    },
    title: {
      text: "",
    },
    xAxis: {
      title: {
        text: "Año",
        align: "high",
      },
      categories: anios /* Aqui va los Años */,
      labels: {
        overflow: "justify",
      },
    },
    yAxis: {
      title: {
        text: "Valor",
      },
    },
    series:series,
  };
  return (
    <>
      <div className="container-fluid" id="StyleGraficoEstructura">
        <div className="container-fluid">
          <HighchartsReact highcharts={Highcharts} options={options} />
        </div>
        <br />
        <div className="table-responsive" id="StyleTabla">
          <Table className="table" striped bordered hover>
            <thead>
              <tr>
                <th>Año</th>
                <th>IBIM</th>
                <th>Calificación</th>
              </tr>
            </thead>
            <tbody>
                {data.map(dato=>
                <tr>
                    <td>{dato.ano}</td>
                    <td>{dato.ibi}</td>
                    <td>{dato.calificacion}</td>

                </tr>
                    )}

            </tbody>
              
          </Table>
        </div>
        <br />
        <div className="btn-group mx-2" role="group" aria-label="segundo boton">
          <Button
            variant="outline-primary"
            type="button"
            value="1"
            onClick={handleClick}
          >
            Limpiar
          </Button>
        </div>
      </div>
    </>
  );
}

export default GrafIbim;
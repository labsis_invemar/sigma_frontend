import React, { useContext } from "react";
import Button from "react-bootstrap/Button";
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import { MapaContext } from "../../context/MapaContext";
import GraficosTabla from "./GraficosTabla";
// import { isFunction } from "formik";

function GrafRegeneracion(props) {
  const { SetEst } = useContext(MapaContext);
  const { setData } = useContext(MapaContext);
  const { data } = useContext(MapaContext);
  const { SetEstadoMapa } = useContext(MapaContext);

/*---Procedimiento para preparar datos para grafica---*/
  let fechas = [];
  let especies = {};
  let keys = Object.keys(data[0])
  let key = null;
  let options = [];
  let series=[]
  const parcela = data[0]['parcela'];
  if(keys.length>3){//si la longitud de la data es > 2 se trata de densidad por especie, hacemos un grafico de barras multiples
    key = Object.keys(data[0])[3];

      
        data.forEach((dato) => {
          
            if (!fechas.includes(dato.fecha)) {
              fechas.push(dato.fecha);
              
            }
            
            if (dato.especie in especies) {
              especies[dato.especie].push(dato[key]);
            } else {
              especies[dato.especie] = [dato[key]];
            }
          
        });
        Object.keys(especies).forEach(esp=>{
          series.push({
              "name":esp,
              "data":especies[esp]
          })
        })

         options = {
          chart: {
            type: "column",
          },
          title: {
            text: key.toUpperCase().replace("DENS", "DENSIDAD").replace("PLANT", "PLANTULAS").replace("PROP", "PROPAGULOS").replace("_", " ").replace("X", " POR").replace("ESP", " ESPECIE").concat(' EN "',parcela,'"'),
          },
          xAxis: {
            title: {
              text: "Fecha",
              align: "high",
            },
            categories: fechas,
            labels: {
              overflow: "justify",
            },
          },
          yAxis: {
            title: {
              text: "Valor",
            },
          },
          series:series,
        };

        
  }else{//si la longitud de la data es < 3 se trata de densidad por estacion, hacemos un grafico de barras simples
    key = Object.keys(data[0])[2];
    
    
    data.forEach((dato)=>{
      if(dato[key] > 0){

        series.push([dato['ano'],dato[key]]);
      }
      
    });

 
  
    options = {
      chart: {
        type: "column",
      },
      title: {
        text: key.toUpperCase().replace("DENS", "DENSIDAD").replace("PLANT", "PLANTULAS").replace("PROP", "PROPAGULOS").replace("_", " ").replace("XESP", "POR ESPECIE").concat(' EN "', parcela,'"'),
      },
      xAxis: {
        type: 'category',
        labels: {
            rotation: -45,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
      yAxis: {
        title: {
          text: "Valor",
        },
      },
      series:[{
        name: key,
        data: series,
        dataLabels: {
          enabled: true,
          rotation: -90,
          color: '#FFFFFF',
          align: 'right',
          format: '{point.y:.3f}', // one decimal
          y: 10, // 10 pixels down from the top
          style: {
              fontSize: '13px',
              fontFamily: 'Verdana, sans-serif'
          }
      }
  }]
  };
  

  }
 
/*-------------------------------------*/


  /* ---------------------------------- */

  const handleClick = () => {
    SetEstadoMapa(false);
    SetEst('reg');
    //setData("LIMPIAR");
    fechas=[];
    especies=[];
    series=[];
  };


  return (
    <>
      <div className="container-fluid" id="StyleGraficoEstructura">
        <div className="container-fluid">
          <HighchartsReact highcharts={Highcharts} options={options} />
        </div>
        <br />
        <div className="table-responsive" >
          <GraficosTabla />
          {/* <Table className="table" striped bordered hover>
            <thead>
              <tr>
                <th>Fecha</th>
                {keys.length>3 &&<th>Especie</th>}
                <th>Valor</th>
                <th>Unidad de <br/> medida</th>
              </tr>
            </thead>
            <tbody>
            
            {data.map(dato=>
                // <tr>
                //     <td>{dato.fecha}</td>
                //     <td>{dato.especie}</td>
                //     <td>{dato[key]}</td>
                //     <td>{"ind/ha"}</td>

                // </tr>
                <tr>
                     <td >{dato.ano}</td>
                     {keys.length>3 && <td key={dato.especie}>{dato.especie}</td> }
                     <td key={dato[key]}>{dato[key]}</td>
                     <td>{key.startsWith('dens')? 'ind/ha':'otro'}</td>
                 </tr>
                )}
                
            </tbody>
              
          </Table> */}
        </div>
        <br />
        <div className="btn-group mx-2" role="group" aria-label="segundo boton">
          <Button
            variant="outline-primary"
            type="button"
            value="1"
            onClick={handleClick}
          >
            Limpiar
          </Button>
        </div>
      </div>
    </>
  );
}

export default GrafRegeneracion;
import React,{ useContext} from 'react'
import { MapaContext } from '../../context/MapaContext'

function GraficoRestauracion(props){
  const {data} = useContext(MapaContext)
  console.log('Esto es la data',data)
  return (
    <>
      <h2>Graficas de restauracion</h2>
      <p>JSON.stringify(data)</p>
    </>
  )
}

export default GraficoRestauracion

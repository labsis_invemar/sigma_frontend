import React, { useContext } from 'react'
import { MapaContext } from '../../context/MapaContext'  
import  '../../assets/style/Table.css'

function GraficosTabla() {
  const { data } = useContext(MapaContext);
  const keys = Object.keys(data[0]);

  //donde debe ir este objeto?, tambien se usa en los formularios
  const unidadesDeMedida = {
      pro_nivel_agua: 'cm ',
      pro_sal_1: 'unid',
      pro_sal_05: 'unid',
      pro_sal_sup: 'unid',
      pro_tem_sup: '°C',
      pro_tem_1: '°C',
      pro_tem_05: '°C',
      pro_ph_sub: 'unid',
      pro_ph_1: 'unid',
      pro_ph_05: 'unid',
      densidad_plant: 'ind/h',
      densidad_prop: 'ind/h',
      densidad_estacion: 'ind/h',
      densidad_absoluta_especie: 'ind/hec',
      abundancia_relativa_especie: 'abundancia',
      area_basal_especie: 'm²',
      area_basal_estacion: 'm²',
      dominancia_relativa: 'dominancia',
      frecuencia_relativa_especie: 'frecuencia',
      indice_valor_importancia: 'importancia',
  };
  
  return (
    <div>
  
  <table className="table" >
  <thead className="table-info">
    <tr>
      <th>Año</th>
      {( keys.includes('especie')) && <th>Especie</th> }
      {( keys.includes('parcela')) && <th>Parcela</th> }
      <th>Valor</th>
      <th>Unidad</th>
      
    </tr>
  </thead>
  <tbody>
  {
   
  data.map(( row ) => {
      
      const variable = keys[keys.length - 1];
              console.log({variable});
                return(
                    <tr>
                          <td >{row.ano}</td>
                         {( keys.includes('especie')) && <td key={row.especie}>{row.especie}</td>  }
                         {( keys.includes('parcela')) && <td key={row.parcela}>{row.parcela}</td>  }
                         <td >{row[variable]}</td>
                         <td>{unidadesDeMedida[variable]}</td>
                     </tr>
                )

            })
  }
    
  </tbody>
</table>
    </div>
  )
}

export default GraficosTabla
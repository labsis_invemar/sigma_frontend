import { React, useEffect, useState, useContext } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLeaf } from "@fortawesome/free-solid-svg-icons/faLeaf";
import { MapaContext } from "../context/MapaContext";
import {
  url_datos_ubicacion_parcela,
  url_datos_ubicacion_parcelas_estacion,
} from "./api";
import esriConfig from "@arcgis/core/config";
import MapView from "@arcgis/core/views/MapView";
import Map from "@arcgis/core/Map";
import MapImageLayer from "@arcgis/core/layers/MapImageLayer";
import GroupLayer from "@arcgis/core/layers/GroupLayer";
import LayerList from "@arcgis/core/widgets/LayerList";
import Legend from "@arcgis/core/widgets/Legend";
import TimeSlider from "@arcgis/core/widgets/TimeSlider";
import GeoJSONLayer from "@arcgis/core/layers/GeoJSONLayer";
import Home from "@arcgis/core/widgets/Home";
import Zoom from "@arcgis/core/widgets/Zoom";
import ui from "@arcgis/core/views/ui/DefaultUI";

import "../assets/images/arbol.png";

const Mapa = () => {
  const { caracterizacion } = useContext(MapaContext);
  const { parcela } = useContext(MapaContext);
  const { est } = useContext(MapaContext);
  const { estadoMapa, multipleParcelas, SerieTiempo } = useContext(MapaContext);

  const [mapa, Setmapa] = useState(); /* Estado mapa */
  const [Caract, setCaract] = useState(); /* Estado Lista */
  const [parc, Setparc] = useState(); /* Estado parcela */
  const [multiple, Setmultiple] = useState(); /* Estado multiples parcelas */
  const [vi, Setvi] = useState(); /* Estado view */
  const [time, Settime] = useState(); /* Estado time Slider */
  const [Group, SetGroup] = useState(); /* Estado Grouplayers */
  const [toribio, Settoribio] = useState(false); /* Estado Toribio */
  let layer = [];

  esriConfig.apiKey =
    "AAPK8ffd672a123d449781552468c6405c78F1dYsg6kSXR2tSRnecvfTuTEoMa_50qFnrHfjwVvolYZWxzwjoCi39ZNzVRctJ3w";

  const MANCOLLayer = new MapImageLayer({
    url: "https://gis.invemar.org.co/arcgis/rest/services/SIGMA/MANGLARES_COLOMBIA/MapServer",
  });
  const SECMANLayer = new MapImageLayer({
    url: "https://gis.invemar.org.co/arcgis/rest/services/SIGMA/SECTORES_MANGLARES/MapServer",
  });

  const BIODIVLayer = new MapImageLayer({
    url: "https://gis.invemar.org.co/arcgis/rest/services/SIGMA/BIODIVERSIDAD/MapServer",
  });
  const ARMAPROLayer = new MapImageLayer({
    title: "AREAS PROTEGIDAS DE MANGLARES",
    url: "https://gis.invemar.org.co/arcgis/rest/services/SIGMA/AMP_SIGMA/MapServer",
  });
  const CGSMLayer = new MapImageLayer({
    title: "SERIES DE TIEMPO CGSM",
    url: "https://gis.invemar.org.co/arcgis/rest/services/SIGMA/CGSM_SERIE_TIEMPO/MapServer",
  });
  const CispataLayer = new MapImageLayer({
    title: "SERIES DE TIEMPO CISPATA",
    url: "https://gis.invemar.org.co/arcgis/rest/services/SIGMA/URRA_SERIE_TIEMPO/MapServer",
  });
  console.log('Cispata Layer',CispataLayer)
  const ZoLayer = new MapImageLayer({
    url: "https://gis.invemar.org.co/arcgis/rest/services/SIGMA/ZONIFICACION/MapServer",
  });
  const ToribioLayer = new MapImageLayer({
    title: "RESTAURACIÓN RIO TORIBIO",
    visible: true,
    url: "https://gis.invemar.org.co/arcgis/rest/services/SIGMA/COBERTURA_RioToribio/MapServer",
  });

  layer = [
    CispataLayer,
    CGSMLayer,
    ARMAPROLayer,
    BIODIVLayer,
    ZoLayer,
    SECMANLayer,
    MANCOLLayer,
  ];

  

  useEffect(() => {

    const ManglarGroupLayer = new GroupLayer({
      title: "SIGMA",
      visible: true,
      visibilityMode: "independent",
      layers: layer,
      opacity: 0.75,
    });

    const map = new Map({
      //basemap: "arcgis-topographic", // Basemap layer service
      //basemap: "osm-dark-gray", // Basemap layer service Basemaps for use with API keys
      basemap: "osm-standard", // Basemap layer service Basemaps for use with API keys (https://developers.arcgis.com/javascript/latest/api-reference/esri-Map.html?msclkid=89face2fc63c11ecac098090661506ba#basemap)
      layers: [ManglarGroupLayer],
      visibilityMode: "independent",
    });

    const view = new MapView({
      map: map,
      center: [-74.2478916, 4.6486259], // Longitude, latitude
      zoom: 6, // Zoom level
      container: "viewDiv", // Div element
    });

    /*----------------------------------------------------------*/

    /* Estados */
    Setmapa(map);
    Setvi(view);
    SetGroup(ManglarGroupLayer.layers);
    /* ------ */

    /*--------------------------------------Lista pleglable de capas----------------*/
    function defineActions(event) {
      var item = event.item;

      if (item.title === "Manglares Nacionales") {
        item.actionsSections = [
          [
            {
              title: "Go to full extent",
              className: "esri-icon-zoom-out-fixed",
              id: "full-extent",
            },
            {
              title: "Layer information",
              className: "esri-icon-description",
              id: "information",
            },
          ],
          [
            {
              title: "Increase opacity",
              className: "esri-icon-up",
              id: "increase-opacity",
            },
            {
              title: "Decrease opacity",
              className: "esri-icon-down",
              id: "decrease-opacity",
            },
          ],
        ];
      }
    }

    view.when(function () {
      const layerList = new LayerList({
        view: view,
        listItemCreatedFunction: function (event) {
          const item = event.item;

          item.panel = {
            content: "legend",
            open: true,
          };
        },

        listItemCreatedFunction: defineActions,
      });

      view.ui.add(layerList, "top-right");
    });

    /*--------------------------------------------------------------------------------*/

    /*--------------------------------Leyenda------------------------------------------*/

    view.when(function () {
      const featureLayer = map.layers.getItemAt(0);

      const legend = new Legend({
        view: view,
        layerInfos: [
          {
            layer: featureLayer,
            title: "Leyenda - Manglares Nacionales",
          },
        ],
      });

      view.ui.add(legend, "bottom-left");
    });

    /*--------------------------------------------------------------------------------*/
    /*------------------Caracterizacion - Cluster Pointer-----------------------------*/

    const clusterConfig = {
      type: "cluster",
      clusterRadius: "100px",
      popupTemplate: {
        title: "Grupos de Parcelas",
        content: "Numero de Parcelas: {cluster_count}",
        fieldInfos: [
          {
            fieldName: "sector_count",
            format: {
              places: 0,
              digitSeparator: true,
            },
          },
        ],
      },
      clusterMinSize: "24px",
      clusterMaxSize: "60px",
      labelingInfo: [
        {
          deconflictionStrategy: "none",
          labelExpressionInfo: {
            expression: "Text($feature.cluster_count, '#,###')",
          },
          symbol: {
            type: "text",
            color: "#004a5d",
            font: {
              weight: "bold",
              family: "Noto Sans",
              size: "12px",
            },
          },
          labelPlacement: "center-center",
        },
      ],
    };

    const template = {
      title: "{parcela}",
      content:
        "Parcela: {parcela} <br/> Id_estacion: {id_estacion} <br/> Estación:{estacion} <br/>Tipo:{tipo} <br/> Zona Protegida:{zonaprotegida} <br/> Sector: {sector} <br/> Región: {region} <br/> Cuenca Hidrografica: {cuencahidrografica} <br/> Categoria unidad de manejo: {categoriaunidadmanejo} <br/> UAC: {uac} <br/> Departamento: {departamento} <br/> CAR: {id_car} <br/> Fecha: {fecha} <br/> Area: {area} <br/> Forma Parcela: {formaparcela} <br/> Latitud: {latitud} <br/> Longitud: {longitud} <br/> Datos Disponibles: {datosdisponibles}",
      fieldInfos: [
        {
          fieldName: "time",
          format: {
            dateFormat: "short-date-short-time",
          },
        },
      ],
    };
    const renderer = {
      type: "simple",
      field: "sector",
      symbol: {
        type: "picture-marker",
        //url: "https://image.flaticon.com/icons/png/512/740/740934.png",
        // url: "https://image.flaticon.com/icons/png/512/2900/2900137.png",
        // height: "32px",
        // width: "32px",
        url: "https://siam.invemar.org.co/static/js/libs/leaflet-0.7.3/images/marker-icon.png",
        height: "41px",
        width: "25px",

      /*   size: 4,
      color: "#69dcff",
      outline: {
        color: "rgba(0,139,174,0.5)",
        width: 5*/
      },
    };
    const CaractLayer = new GeoJSONLayer({
      title: "Caracterización",
      //url: "https://devsiam.invemar.org.co/api-sigma/caract-gis",
      url: `${process.env.REACT_APP_API_URL}/estructura/caract-gis`,
      //url: "http://geovisorsigma.invemar.org.co/api/caract-gis",
      copyright: "INVEMAR",
      featureReduction: clusterConfig,
      popupTemplate: template,
      renderer: renderer, //optional
    });

    setCaract(CaractLayer);
    console.log(Caract);
    /*----------------------------------------------------------------------------- */
    /*----------------------------Pintar Parcela----------------------------------- */
    const templateParcela = {
      title: "{nombre}",
      content: "Parcela: {nombre} ",
      fieldInfos: [
        {
          fieldName: "time",
          format: {
            dateFormat: "short-date-short-time",
          },
        },
      ],
    };

    const rendererParcela = {
      type: "simple",
      field: "sector",
      symbol: {
        type: "picture-marker",
        //url: "https://image.flaticon.com/icons/png/512/740/740934.png",
        // url: "https://image.flaticon.com/icons/png/512/6188/premium/6188092.png",
        url: "https://siam.invemar.org.co/static/js/libs/leaflet-0.7.3/images/marker-icon.png",
        height: "41px",
        width: "25px",

        /* size: 4,
      color: "#69dcff",
      outline: {
        color: "rgba(0,139,174,0.5)",
        width: 5,
      }, */
      },
    };

    const PuntoLayer = new GeoJSONLayer({
      title: "Parcela",
      url: url_datos_ubicacion_parcela(parcela),
      copyright: "INVEMAR",
      popupTemplate: templateParcela,
      renderer: rendererParcela,
    });
    /*-----------------------------------------------------------------*/

    /*------------ Multiples parcelas ---------------*/
    const MultipleLayer = new GeoJSONLayer({
      title: "Parcelas",
      url: url_datos_ubicacion_parcelas_estacion(parcela),
      copyright: "INVEMAR",
      featureReduction: clusterConfig,
      popupTemplate: templateParcela,
      renderer: rendererParcela,
    });

    Setparc(PuntoLayer);
    Setmultiple(MultipleLayer);
    /*------------------------------------------------*/

    /*------ Boton Home--------*/
    view.ui.add(
      new Home({
        view: view,
      }),
      "top-left"
    );
    /*-------------------------*/

    /*---------------------------- Time Slider ------------------------*/

    const timeSlider = new TimeSlider({
      container: "timeSlider",
      view: view,
      timeVisible: true,
      loop: true,
    });
    /*-----------------------------------------------------------------*/

    /* ------------------------Time Slider CGSM----------------------- */
    if (map.layers !== null) {
      view.when(CGSMLayer).then((lv) => {
        console.log('CGSMLayer',CGSMLayer.timeInfo.fullTimeExtent);
        if (CGSMLayer.timeInfo.fullTimeExtent !== null) {
          timeSlider.fullTimeExtent =
            CGSMLayer.timeInfo.fullTimeExtent.expandTo("years");
          timeSlider.stops = {
            interval: CGSMLayer.timeInfo.interval,
          };
        }
      });
    }

    /* ---------------------------------------------------------------- */

    /* ------------------------Time Slider CISPATA----------------------- */
    if (map.layers !== null) {
    
      view.when(CispataLayer).then((lv) => {
        if (CispataLayer.timeInfo.fullTimeExtent !== null) {
          timeSlider.fullTimeExtent =
            CispataLayer.timeInfo.fullTimeExtent.expandTo("years");
          timeSlider.stops = {
            interval: CispataLayer.timeInfo.interval,
          };
        }
      });
    }

    /* ---------------------------------------------------------------- */

    Settime(timeSlider);
  }, [parcela]);

  function zoomToLayer(layer) {
    return layer.when(() => {
      vi.goTo(layer.fullExtent);
    });
  }

  /* Logica de activación de capas y marcadores del mapa */

  useEffect(() => {
    if (mapa) {
      if (caracterizacion === true) {
        mapa.add(Caract);
      } else {
        mapa.remove(Caract);
      }
    }
  }, [caracterizacion]);

  /*------------------------------------------------------*/

  /* Activar parcela segun la metodologia */
  useEffect(() => {
    if (mapa) {
      if (estadoMapa === true) {
        console.log("Llega");
        mapa.add(parc);
        zoomToLayer(parc);
      } else {
        console.log("se fue");
        mapa.remove(parc);
      }
    }
  }, [estadoMapa]);
  /* ------------------------------------ */

  /* Activar grupo de parcelas segun la metodologia */
  useEffect(() => {
    if (mapa) {
      if (multipleParcelas === true) {
        console.log("Llega");
        mapa.add(multiple);
        zoomToLayer(multiple);
      } else {
        console.log("se fue");
        mapa.remove(multiple);
      }
    }
  }, [multipleParcelas]);
  /* ------------------------------------ */

  /* Logica para activar Time Slider */

  useEffect(() => {
    if (mapa) {
      if (SerieTiempo === true) {
        vi.ui.add(time, "bottom-left");
      } else {
        vi.ui.remove(time, "bottom-left");
      }
    }
  }, [SerieTiempo]);

  return (
    <>
      <div className="justify-content-center w-100" id="viewDiv">
        {SerieTiempo ? (
          <>
            {SerieTiempo === true && (
              <>
                <div id="timeSlider" />
                <div id="titleDiv" className="esri-widget" />
              </>
            )}
          </>
        ) : (
          []
        )}
      </div>
    </>
  );
};

export default Mapa;

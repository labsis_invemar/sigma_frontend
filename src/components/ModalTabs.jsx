import React from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import Media from "react-bootstrap/Media";
import imagengeo from "../assets/images/imagen-geovisor535x380.png";
import "../assets/style/Modal.css";

const ModalTabs = ({ show, handleClose }) => {
  return (
    <>
      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
        size="lg"
        arial-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header id="StyleBorderTab">
          <Modal.Title
            id="contained-modal-title-vcenter"
            variant="StyleTabColor"
          >
            Acerca de...
          </Modal.Title>
          <Button variant="close" onClick={handleClose}></Button>
        </Modal.Header>
        <Modal.Body>
          <Tabs defaultActiveKey="home" id="uncontrolled-tab-example">
            <Tab eventKey="home" title="Acerca de">
              <Media>
                <img
                  src={imagengeo}
                  alt="Generic placeholder"
                  height="100%"
                  width="100%"
                />
              </Media>
              {/* <Sonnet /> */}
            </Tab>
            <Tab eventKey="Intro" title="Introducción">
              <p align="justify">
                El Sistema de Información para la Gestión de los Manglares de
                Colombia (SIGMA), es un proyecto ambicioso de carácter nacional
                e interinstitucional, que pretende fomentar el conocimiento del
                ecosistema de manglar en el país, así como contribuir a la toma
                de decisiones a través de la generación de reportes basados en
                cuatro teméticas básicas a saber: Estado de los bosques;
                Presiones; Bienes y Servicios y Gestión. Como parte de las
                herramientas transversales que acompañan el SIGMA se diseño el
                presente Geovisor; un servicio en línea que provee la
                información cartográfica disponible en cuanto a coberturas de
                manglar y aspectos complementarios tales como biodiversidad
                asociada, amenazas, zonas bajo alguna categoría de protección,
                entre otros de interés. La herramienta además ofrece consultas
                especiales relacionadas con el cambio de la cobertura de la
                tierra para algunas áreas de manglar. El geovisor, no es una
                herramienta especializada para usuarios SIG, y puede ser
                consultada por usuarios con distintos roles (tomadores de
                decisiones, investigadores, autoridades ambientales, comunidad
                en general, etc.), dado su fácil uso y acceso. Consulte en el
                Manual de Usuario del SIGMA el capítulo correspondiente al
                manejo del Geovisor, a través de: (Manual del usuario) Para más
                información: http://sigma.invemar.org.co (c) Copyright 2014
                INVEMAR. Todos Los Derechos Reservados.
              </p>
              {/* <Sonnet /> */}
            </Tab>
            <Tab eventKey="info" title="Información Generales">
              <p align="justify">
                La información aquí expuesta proviene de diferentes fuentes
                (instituciones) la cual puede ser consultada en los metadatos.
                Para la generación de los geoservicios se utilizó el software
                ArcGIS Server 10.2.1 y para el desarrollo del geovisor se
                utilizó la API de FLEX para ArcGIS Server 3.6. Las capas del
                geovisor están definidas en el sistema de coordenadas geográfico
                usando el datum “MAGNA”; adicionalmente se diseño el geovisor
                acorde a los estándares internacionales, lo cual permite que los
                usuarios SIG puedan cargar el geoservicio desde sus herramientas
                de escritorio y aplicaciones mediante un servicio de mapas web
                que soporten servicios WMS (preguntar a
                administrador.sigma@invemar.org.co).
              </p>
              {/* <Sonnet /> */}
            </Tab>
            <Tab eventKey="Cons" title="Consideraciones de uso">
              <p align="justify">
                No se garantiza al usuario la precisión global de los datos, es
                advertido de posibles errores en los datos suministrados, y
                asume la responsabilidad de realizar los chequeos necesarios
                para detectarlos, corregirlos e interpretarlos de manera
                correcta. El usuario hace uso de los datos bajo su propia
                responsabilidad y acepta las limitaciones existentes en los
                datos. El usuario debe reconocer que el conjunto de datos
                suministrado está sujeto a las leyes que reglamentan los
                derechos de autor y de propiedad intelectual y se comprometen a
                respetarlas. Si tiene alguna sugerencia o comentario por favor
                contáctese con administrador.sigma@invemar.org.co Créditos:
                INVEMAR 2015, GEZ:LabSIS.
              </p>
              {/* <Sonnet /> */}
            </Tab>
          </Tabs>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="success" href="http://sigma.invemar.org.co" target="_blank">Portal SIGMA</Button>
          <Button variant="primary" href="mailto:administrador.sigma@invemar.org.co?subject=Geovisor">Contacto</Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default ModalTabs;

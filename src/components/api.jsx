const dev = 'http://localhost:8300/api';
const prod = 'https://geovisorsigma.invemar.org.co/api';

// process.env.REACT_APP_URL_API
console.log(process.env.REACT_APP_URL_API)

//Estrutura
export const url_departamento_estructura =`${dev}/estructura/departamentos`;
export const url_municipio_estructura = ((id_dep) => `${dev}/estructura/municipios/${id_dep}/`);
export const url_estacion_estructura = ((id_mun) => `${dev}/estructura/estaciones/${id_mun}/`);
export const url_parcela_estructura = ((id_est) => `${dev}/estructura/parcelas/${id_est}/`);
export const url_especie_estructura  = ((id_par) => `${dev}/estructura/especies/${id_par}/`);
export const url_datos_estructura_especie = ((id_par,id_esp,id_var) => `${dev}/estructura/consulta-variables/${id_par}/${id_esp}/${id_var}/`);
// export const url_datos_estructura_especie_variable = ((id_esp,variable) => `${dev}/estructura/datos-especie-variable/${id_esp}/${variable}/`);

//Fisicoquimico
export const url_departamento_fq = `${dev}/fisicoquimico/departamentos`;
export const url_municipio_fq = ((id_dep)=>`${dev}/fisicoquimico/municipios/${id_dep}/`);
export const url_estacion_fq = ((id_munic)=>`${dev}/fisicoquimico/estaciones/${id_munic}/`);
export const url_datos_fq_estacion = ((id_est,id_var)=>`${dev}/fisicoquimico/data-estaciones/${id_est}/${id_var}/`);


//Regeneracion
export const url_departamento_reg = `${dev}/regeneracion/departamentos/`;
export const url_municipio_reg = ((id_dep) =>`${dev}/regeneracion/municipios/${id_dep}/`);
export const url_estacion_reg =((id_munic) => `${dev}/regeneracion/estaciones/${id_munic}/`); 
export const url_parcela_reg = ((id_est) => `${dev}/regeneracion/parcelas/${id_est}/`);
export const url_especie_reg = ((id_parc)=> `${dev}/regeneracion/especies/${id_parc}/`);
export const url_anios_reg = ((id_parc, variable)=> `${dev}/regeneracion/anios/${id_parc}/${variable}/`);
export const url_datos_reg = ((id_anio,id_parc,variable)=> `${dev}/regeneracion/consulta-variables/${id_anio}/${id_parc}/${variable}/`);


//Ibim
export const url_departamento_ibim = `${dev}/get-departamento-ibim/`;
export const url_estacion_ibim = ((id_dep)=> `${dev}/get-estacion-ibim/${id_dep}/`);
export const url_datos_ibim = ((id_est)=> `${dev}/get-ibim/${id_est}/`);

//Geografico
export const url_datos_ubicacion_parcela = ((id_par)=>`${dev}/get-ubicacion-parcela/${id_par}/`);//hacer fetch para enviar datos con consulta
export const url_datos_ubicacion_parcelas_estacion = ((id_est)=>`${dev}/get-ubicacion-parcela-estacion/${id_est}/`);


//Restauracion
export const url_datos_siembra = `${dev}/contador-arboles/`;
export const url_datos_restauracion_estacion = ((anio,id_est)=> `${dev}/restauracion/restauracion-data/${anio}/${id_est}/total/`);
export const url_data_general_restauracion = ((anio,id_proy,id_est)=> `${dev}/restauracion/data-general-proyecto/${anio}/${id_proy}/${id_est}/`);
export const url_siembraxdepartamento_restauracion = `${dev}/restauracion/siembra-departamentos/`;
export const url_areas_restauracion = `${dev}/restauracion/areas-restauracion/`;

//Opciones del formulario
export const url_departamento_restauracion = `${dev}/restauracion/departamentos`;
export const url_municipio_restauracion = ((id_dep)=> `${dev}/restauracion/municipios/${id_dep}/`);
export const url_proyecto_restauracion = ((id_munic)=> `${dev}/restauracion/proyectos/${id_munic}/`);
export const url_proyecto_restauracion_todos = `${dev}/restauracion/proyectos/todos/`;
export const url_estacion_restauracion = ((id_dep,id_proj)=> `${dev}/restauracion/estaciones/${id_dep}/${id_proj}/`);
//export const url_especies_estacion_restauracion = ((id_est)=> `${dev}/restauracion/especies${id_est}/`);
export const url_anios_restauracion = ((id_est,id_proj)=> `${dev}/restauracion/anios/${id_est}/${id_proj}/`);



//variables de respuesta
export const url_dens_prop_restauracion = ((id_est,anio)=> `${dev}/densidad-propagulos/${id_est}/${anio}/`);
export const url_dens_plant_restauracion = ((id_est,anio)=> `${dev}/densidad-plantulas/${id_est}/${anio}/`);
export const url_mortalidad_estacion = ((id_est)=> `${dev}/mortalidad-estacion/${id_est}/`);
export const url_supervivencia_estacion = ((id_est)=> `${dev}/supervivencia-estacion/${id_est}/`);


// export const url_mortalidad_especie = ((id_est,anio)=> `${dev}/mortalidad-especie/${id_est}/${anio}/`);
// export const url_supervivencia_especie = ((id_est,anio)=> `${dev}/SupervivenciaxEsp/${id_est}/${anio}/`);

// export const url_datos_siembra = `${process.env.REACT_APP_URL_API}/contadorArboles/`;

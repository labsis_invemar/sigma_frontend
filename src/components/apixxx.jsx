//Estrutura
export const url_departamento_estructura ='https://devsiam.invemar.org.co/api-sigma/get-departamento';

export const url_estacion_estructura = ((id_dep) => `https://devsiam.invemar.org.co/api-sigma/get-estacion/${id_dep}/`);

export const url_parcela_estructura = ((id_est) => `https://devsiam.invemar.org.co/api-sigma/get-parcelas/${id_est}/`);

export const url_especie_estructura  = ((id_par) => `https://devsiam.invemar.org.co/api-sigma/get-especie/${id_par}/`);

export const url_datos_estructura_especie = ((id_par,id_esp,id_var) => `https://devsiam.invemar.org.co/api-sigma/get-estructura-especie-variable/${id_par}/${id_esp}/${id_var}/`);

export const url_datos_estructura_especie_variable = ((id_esp,variable) => `https://devsiam.invemar.org.co/api-sigma/get-datos-especie-variable/${id_esp}/${variable}/`);


//Fisicoquimico
export const url_departamento_fq = 'https://devsiam.invemar.org.co/api-sigma/departamentofq';
export const url_estacion_fq = ((id_dep)=>`https://devsiam.invemar.org.co/api-sigma/estacionesfq/${id_dep}/`);
export const url_datos_fq_estacion = ((id_est,id_var)=>`https://devsiam.invemar.org.co/api-sigma/all-fq-estacion/${id_est}/${id_var}/`);


//Regeneracion
export const url_departamento_reg = 'https://devsiam.invemar.org.co/api-sigma/get-departamento-regeneracion/';
export const url_estacion_reg =((id_dep) => `https://devsiam.invemar.org.co/api-sigma/get-estacion-regeneracion/${id_dep}/`); 
export const url_parcela_reg = ((id_est) => `https://devsiam.invemar.org.co/api-sigma/get-parcela-regeneracion/${id_est}/`);
export const url_especie_reg = ((id_parc)=> `https://devsiam.invemar.org.co/api-sigma/get-especie-regeneracion/${id_parc}/`);
export const url_datos_reg = ((id_esp,id_parc,variable)=> `https://devsiam.invemar.org.co/api-sigma/get-all-regeneracion/${id_esp}/${id_parc}/${variable}/`);


//IBIM
export const url_departamento_ibim = 'https://devsiam.invemar.org.co/api-sigma/get-departamento-ibim/';
export const url_estacion_ibim = ((id_dep)=> `https://devsiam.invemar.org.co/api-sigma/get-estacion-ibim/${id_dep}/`);
export const url_datos_ibim = ((id_est)=> `https://devsiam.invemar.org.co/api-sigma/get-ibim/${id_est}/`);

//Geografico
export const url_datos_ubicacion_parcela = ((id_par)=>`https://devsiam.invemar.org.co/api-sigma/get-ubicacion-parcela/${id_par}/`);//hacer fetch para enviar datos con consulta
export const url_datos_ubicacion_parcelas_estacion = ((id_est)=>`https://devsiam.invemar.org.co/api-sigma/get-ubicacion-parcela-estacion/${id_est}/`);
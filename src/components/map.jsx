import React, { useEffect, useState, useReducer, useContext } from "react";
import { loadModules } from "esri-loader";
import { url_datos_ubicacion_parcela } from "../components/api";

import "../assets/style/Map.css";

import { MapaContext } from "../context/MapaContext";

const Mapa = ({ estado }) => {
  const [mapa, setMapa] = useState(null);
  const [vi, SetVi] = useState(null);
  const [caractLayer, setCaractLayer] = useState(null);
  const [punto, setPunto] = useState([]);


  /* Restauración  */
  const [Ctoribio, SetCToribio] = useState([]);
  

  /* Carga de puntos al mapa */
  const [Lat, setLat] = useState([]);
  const [Lon, setLon] = useState([]);
  const [Act, setAct] = useState([]);



  const { data } = useContext(MapaContext);
  const { est } = useContext(MapaContext);
  const { parcela } = useContext(MapaContext);
  const { SetParcela } = useContext(MapaContext);
  const { estadoMapa } = useContext(MapaContext);
  const { SetEstadoMapa } = useContext(MapaContext);
  const { capaToribio } = useContext(MapaContext);

  let layers = [];
  let activar = [];

  /*-------------------------Caso Toribio--------------------------------- */

  if (capaToribio === true) {
    console.log("activo");
    layers.push(Ctoribio);
  } else {
    if (capaToribio === false) {
      console.log("desactivo");
      layers.pop(Ctoribio);
    }
  }

  /*---------------------------------------------------------------------- */

  useEffect(() => {
    loadModules([
      "esri/config",
      "esri/Map",
      "esri/views/MapView",
      "esri/tasks/Locator",
      "esri/layers/FeatureLayer",
      "esri/layers/MapImageLayer",
      "esri/layers/GroupLayer",
      "esri/widgets/LayerList",
      "esri/widgets/Legend",
      "esri/widgets/Feature",
      "esri/widgets/TimeSlider",
      "esri/layers/GeoJSONLayer",
      "esri/Graphic",
    ])
      .then(
        ([
          esriConfig,
          Map,
          MapView,
          Locator,
          FeatureLayer,
          MapImageLayer,
          GroupLayer,
          LayerList,
          Legend,
          Feature,
          TimeSlider,
          GeoJSONLayer,
          Graphic,
        ]) => {
          esriConfig.apiKey =
            "AAPK8ffd672a123d449781552468c6405c78F1dYsg6kSXR2tSRnecvfTuTEoMa_50qFnrHfjwVvolYZWxzwjoCi39ZNzVRctJ3w";

          /*------------------------------------------GeoServer---------------------------------------------- */
          const MANCOLLayer = new MapImageLayer({
            url: "https://gis.invemar.org.co/arcgis/rest/services/SIGMA/MANGLARES_COLOMBIA/MapServer",
          });
          const SECMANLayer = new MapImageLayer({
            url: "https://gis.invemar.org.co/arcgis/rest/services/SIGMA/SECTORES_MANGLARES/MapServer",
          });

          const BIODIVLayer = new MapImageLayer({
            url: "https://gis.invemar.org.co/arcgis/rest/services/SIGMA/BIODIVERSIDAD/MapServer",
          });
          const ARMAPROLayer = new MapImageLayer({
            url: "https://gis.invemar.org.co/arcgis/rest/services/SIGMA/AMP_SIGMA/MapServer",
          });
          const CGSMLayer = new MapImageLayer({
            url: "https://gis.invemar.org.co/arcgis/rest/services/SIGMA/CGSM_SERIE_TIEMPO/MapServer",
          });
          const CispataLayer = new MapImageLayer({
            url: "https://gis.invemar.org.co/arcgis/rest/services/SIGMA/URRA_SERIE_TIEMPO/MapServer",
          });
          const ZoLayer = new MapImageLayer({
            url: "https://gis.invemar.org.co/arcgis/rest/services/SIGMA/ZONIFICACION/MapServer",
          });

          /*------------------------------------Caso toribio--------------------------------------------*/

          const ToribioLayer = new MapImageLayer({
            title: "Restauración Rio Toribio",
            visible: false,
            url: "https://gis.invemar.org.co/arcgis/rest/services/SIGMA/COBERTURA_RioToribio/MapServer",
          });
          /*--------------------------------------------------------------------------------------------*/

          layers = [
            ToribioLayer,
            CispataLayer,
            CGSMLayer,
            ARMAPROLayer,
            BIODIVLayer,
            ZoLayer,
            SECMANLayer,
            MANCOLLayer,
          ];

          const ManglarGroupLayer = new GroupLayer({
            title: "SIGMA",
            visible: true,
            visibilityMode: "independent",
            layers: layers,
            opacity: 0.75,
          });

          /*------------------------------------------------------------------------------------------------ */

          /*------------------Caracterizacion - Cluster Pointer-----------------------------*/

          const clusterConfig = {
            type: "cluster",
            clusterRadius: "100px",
            popupTemplate: {
              title: "Grupos de Parcelas",
              content: "Numero de Parcelas: {cluster_count}",
              fieldInfos: [
                {
                  fieldName: "sector_count",
                  format: {
                    places: 0,
                    digitSeparator: true,
                  },
                },
              ],
            },
            clusterMinSize: "24px",
            clusterMaxSize: "60px",
            labelingInfo: [
              {
                deconflictionStrategy: "none",
                labelExpressionInfo: {
                  expression: "Text($feature.cluster_count, '#,###')",
                },
                symbol: {
                  type: "text",
                  color: "#004a5d",
                  font: {
                    weight: "bold",
                    family: "Noto Sans",
                    size: "12px",
                  },
                },
                labelPlacement: "center-center",
              },
            ],
          };

          const template = {
            title: "{parcela}",
            content:
              "Parcela: {parcela} <br/> Id_estacion: {id_estacion} <br/> Estación:{estacion} <br/>Tipo:{tipo} <br/> Zona Protegida:{zonaprotegida} <br/> Sector: {sector} <br/> Región: {region} <br/> Cuenca Hidrografica: {cuencahidrografica} <br/> Categoria unidad de manejo: {categoriaunidadmanejo} <br/> UAC: {uac} <br/> Departamento: {departamento} <br/> CAR: {id_car} <br/> Fecha: {fecha} <br/> Area: {area} <br/> Forma Parcela: {formaparcela} <br/> Latitud: {latitud} <br/> Longitud: {longitud} <br/> Datos Disponibles: {datosdisponibles}",
            fieldInfos: [
              {
                fieldName: "time",
                format: {
                  dateFormat: "short-date-short-time",
                },
              },
            ],
          };
          const renderer = {
            type: "simple",
            field: "sector",
            symbol: {
              type: "simple-marker",
              size: 4,
              color: "#69dcff",
              outline: {
                color: "rgba(0,139,174,0.5)",
                width: 5,
              },
            },
          };

          const CaractLayer = new GeoJSONLayer({
            title: "Caracterización",
            url: `${process.env.REACT_APP_API_URL}/caract-gis`,
            copyright: "INVEMAR",
            featureReduction: clusterConfig,
            popupTemplate: template,
            renderer: renderer, //optional
          });

          /*--------------------------------------------------------------------------------*/

          /*-----------------------------------------Carga de punto------------------------------------*/

          const PuntoParcela = new GeoJSONLayer({
            title: "Parcela",
            url: url_datos_ubicacion_parcela(parcela),
          });

          /*-------------------------------------------------------------------------------------------*/

          /*-------------------------------------Mapa Base---------------------------------------- */

          const map = new Map({
            basemap: "arcgis-topographic", // Basemap layer service
            layers: [ManglarGroupLayer],
            visibilityMode: "independent",
          });

          const view = new MapView({
            map: map,
            center: [-74.2478916, 4.6486259], // Longitude, latitude
            zoom: 6, // Zoom level
            container: "viewDiv", // Div element
          });

          /* Variables de estado */
          setCaractLayer(CaractLayer);
          setMapa(map);
          SetVi(view);
          setPunto(PuntoParcela);

          /*--------------------------------------Lista pleglable de capas----------------*/
          function defineActions(event) {
            var item = event.item;

            if (item.title === "Manglares Nacionales") {
              item.actionsSections = [
                [
                  {
                    title: "Go to full extent",
                    className: "esri-icon-zoom-out-fixed",
                    id: "full-extent",
                  },
                  {
                    title: "Layer information",
                    className: "esri-icon-description",
                    id: "information",
                  },
                ],
                [
                  {
                    title: "Increase opacity",
                    className: "esri-icon-up",
                    id: "increase-opacity",
                  },
                  {
                    title: "Decrease opacity",
                    className: "esri-icon-down",
                    id: "decrease-opacity",
                  },
                ],
              ];
            }
          }

          view.when(function () {
            const layerList = new LayerList({
              view: view,
              listItemCreatedFunction: function (event) {
                const item = event.item;

                item.panel = {
                  content: "legend",
                  open: true,
                };
              },

              listItemCreatedFunction: defineActions,
            });

            view.ui.add(layerList, "top-right");
          });

          /*--------------------------------------------------------------------------------*/

          /*--------------------------------Leyenda------------------------------------------*/

          view.when(function () {
            const featureLayer = map.layers.getItemAt(0);

            const legend = new Legend({
              view: view,
              layerInfos: [
                {
                  layer: featureLayer,
                  title: "Leyenda - Manglares Nacionales",
                },
              ],
            });

            view.ui.add(legend, "bottom-left");
          });

          /*--------------------------------------------------------------------------------*/

          /*---------------------------TimeSlider------------------------------------------ */

          const timeSlider = new TimeSlider({
            container: "timeSlider",
            view: view,
            timeVisible: true,
            loop: true,
          });

          /*----------------Cispata-------------------*/
          view.whenLayerView(CispataLayer).then((lv) => {
            timeSlider.fullTimeExtent =
              CispataLayer.timeInfo.fullTimeExtent.expandTo("years");
            timeSlider.stops = {
              interval: CispataLayer.timeInfo.interval,
            };
          });

          /*--------------------CGSM----------------------*/
          view.whenLayerView(CGSMLayer).then((lv) => {
            timeSlider.fullTimeExtent =
              CGSMLayer.timeInfo.fullTimeExtent.expandTo("years");
            timeSlider.stops = {
              interval: CGSMLayer.timeInfo.interval,
            };
          });
          view.ui.add(timeSlider, "manual");
          view.ui.add("titleDiv", "top-right");

          document.getElementById("timeSlider").style.display = "none";
          document.getElementById("titleDiv").style.display = "none";
          /*--------------------------------------------------------------------------------*/

          /* Prueba de carga de punto */

          const point = {
            type: "point",
            longitude: Lat,
            latitude: Lon,
          };

          const markerSymbol = {
            type: "simple-marker",
            color: [226, 119, 40],
            outline: {
              color: [255, 255, 255],
              width: 2,
            },
          };

          const pointGraphic = new Graphic({
            geometry: point,
            symbol: markerSymbol,
          });

          
          if (Act === true) {
            console.log('llego')
            console.log(Lat);
            console.log(Lon);
            view.graphics.add(pointGraphic);  
          }else if (Act === false) {
            console.log('Se fue')
            view.graphics.remove(pointGraphic);
          }
          
        }
      )
      .catch((err) => {
        console.log(err);
      });
  }, []);

  /* Cambio de estado para cargar el geoJson de caracterización */
  if (mapa) {
    if (!estado) {
      mapa.add(caractLayer);
    } else {
      mapa.remove(caractLayer);
    }
  }
  /*-----------------------------------------------------------*/

  /* Cambio de estado para cargar punto de parcela */

  useEffect(() => {
    if (estadoMapa === true) {
      console.log('SI')
      setLat(4.6486259);
      setLon(-74.2478916);
      setAct(true);
    } else {
      if (estadoMapa === false) {
        console.log('no')
        setAct(false);
      }
    }
  }, [estadoMapa]);



  

  /*---------------------------------------------- */

  /*---------Cambio de estado para series de tiempo ---------- */

  useEffect(() => {
    if (mapa) {
      if (data === "activar") {
        document.getElementById("timeSlider").style.display = "block";
        document.getElementById("titleDiv").style.display = "block";
      }
      if (data === "Noactivar") {
        document.getElementById("timeSlider").style.display = "none";
        document.getElementById("titleDiv").style.display = "none";
      }
    }
  }, [data]);

  return (
    <>
      <div className="justify-content-center w-100" id="viewDiv">
        <div id="timeSlider" />
        <div id="titleDiv" className="esri-widget" />
      </div>
    </>
  );
};

export default React.memo(Mapa);

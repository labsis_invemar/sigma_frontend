import React, { useState, useContext, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faMapMarker,
  faArrowsAltV,
  faQrcode,
  faSeedling,
  faTree,
  faClock,
  faChartBar,
} from "@fortawesome/free-solid-svg-icons";
import { faPagelines } from "@fortawesome/free-brands-svg-icons";
import Button from "react-bootstrap/Button";
import FormCaracterizacion from "./Forms/FormsCaracterizacion";
import FormEstrutura from "./Forms/FormsEstructura";
import FormRegeneracion from "./Forms/FormsRegeneracion";
import FormFisicoquimico from "./Forms/FormsFq";
import FormRestauracion from "./Forms/FormsRestauracion";
import FormIBim from "./Forms/FormsIndicadorIBIm";
import GrafEstruc from "./Grafic/GrafEstructura";
import GraficoFq from "./Grafic/GraficoFisicoQuimico";
import FormSeriesCispata from "./Forms/FormsSeriesCispata";
import FormsCGSM from "./Forms/FormsCGSM";

import { MapaContext } from "../../src/context/MapaContext";

import "../assets/style/Barra.css";
import "../assets/style/Menu.css";
import "../index.css";
import "../assets/style/graf.css";
import GrafRegeneracion from "./Grafic/GraficoRegeneracion";
import GrafIbim from "./Grafic/GraficasIbim";
import GraficoRestauracion from "./Grafic/GraficoRestauracion";

function Menu({ handle, SetEstado }) {
  const { SetEst } = useContext(MapaContext);
  const { est } = useContext(MapaContext);
  const { setData } = useContext(MapaContext);
  const { Setcaracterizacion } = useContext(MapaContext);
  const {SetmultipleParcelas}=useContext(MapaContext);
  const { SetSerieTiempo,SerieTiempo} = useContext(MapaContext);

  let estrutura_modulo;
  let fq_modulo;
  let regeneracion_modulo;
  let ibim_modulo;

  if (est === "est") {
    estrutura_modulo = <GrafEstruc />;
  }
  if (est === "Noest") {
    estrutura_modulo = <FormEstrutura />;
  }

  if (est === "reg") {
    regeneracion_modulo = <FormRegeneracion />;
  }
  if (est === "Noreg") {
    regeneracion_modulo = <GrafRegeneracion />;
  }
  if (est === "ibim") {
    ibim_modulo = <FormIBim />;
  }
  if (est === "Noibim") {
    ibim_modulo = <GrafIbim />;
  }

  if (est === "fq") {
    fq_modulo = <FormFisicoquimico />;
  }
  if (est === "Nofq") {
    fq_modulo = <GraficoFq />;
  }
  if (est === "res") {
    fq_modulo = <FormRestauracion />;
  }
  if (est === "Nores") {
    fq_modulo = <GraficoRestauracion />;
  }

  /*--------------------Habilitar y Deshabilitar panel-----------------------*/
  const [modulo, setModulo] = useState(null);
  const { SetEstadoMapa } = useContext(MapaContext);
  const { SetEstRestauracion } = useContext(MapaContext);

  const handleClick = (e) => {
    SetEstado(true);
    
    console.log(e.target.text);
    setModulo(e.target.text);/* Para actividar el modulo al dar clic sobre la opción en el sideBar */
    SetEstRestauracion(false);/* Para hacer el cambio de estado por defecto del modulo de consulta de restauracion */

    switch (e.target.text) {
      case "EstadoEstructura":
        SetEst("Noest");
        break;

      case "EstadoFisicoquimico":
        SetEst("fq");
        break;

      case "EstadoRegeneraciónNatural":
        SetEst("reg");
        break;

      case "IndicadorIBIm":
        SetEst("ibim");
        break;

      case "Restauración":
        SetEst("res");
        break;

      default:
        break;
    }
  };




  /*----------------------------------------------------------------------------*/

  return (
    <>
      <div className="sidebar">
        <a
          className="StyleBorderMenu"
          onClick={(e) => {
            handleClick(e);
            SetEstadoMapa(false);
            SetmultipleParcelas(false);
            SetSerieTiempo(false);
          }}
        >
          <FontAwesomeIcon icon={faMapMarker} />
          <FontAwesomeIcon icon={faPagelines} />
          <br />
          Caracterización
        </a>
        <a
          className="StyleBorderMenu"
          onClick={(e) => {
            handleClick(e);
            Setcaracterizacion(false);
            SetmultipleParcelas(false);
            SetEstadoMapa(false);
            SetSerieTiempo(false);
          }}
        >
          <FontAwesomeIcon icon={faArrowsAltV} />
          <FontAwesomeIcon icon={faPagelines} />
          <br />
          Estado
          <br />
          Estructura
        </a>

        <a
          onClick={(e) => {
            handleClick(e);
            Setcaracterizacion(false);
            SetEstadoMapa(false);
            SetmultipleParcelas(false);
            SetSerieTiempo(false);
          }}
        >
          <FontAwesomeIcon icon={faQrcode} />
          <FontAwesomeIcon icon={faPagelines} />
          <br />
          Estado
          <br />
          Fisicoquimico
        </a>

        <a
          onClick={(e) => {
            handleClick(e);
            Setcaracterizacion(false);
            SetEstadoMapa(false);
            SetmultipleParcelas(false);
            SetSerieTiempo(false);
          }}
        >
          <FontAwesomeIcon icon={faSeedling} />
          <FontAwesomeIcon icon={faPagelines} />
          <br />
          Restauración
        </a>

        <a
          onClick={(e) => {
            handleClick(e);
            Setcaracterizacion(false);
            SetEstadoMapa(false);
            SetmultipleParcelas(false);
            SetSerieTiempo(false);
          }}
        >
          <FontAwesomeIcon icon={faArrowsAltV} />
          <FontAwesomeIcon icon={faPagelines} />
          <br />
          Estado
          <br />
          Regeneración
          <br />
          Natural
        </a>

        <a href="http://cinto.invemar.org.co/egreta/" target="_blank">
          <FontAwesomeIcon icon={faTree} />
          <FontAwesomeIcon icon={faPagelines} />
          <br />
          Presiones
        </a>

        <a
          onClick={(e) => {
            handleClick(e);
            Setcaracterizacion(false);
            SetEstadoMapa(false);
            SetmultipleParcelas(false);
            SetSerieTiempo(false);
          }}
          /*href="https://invemar.maps.arcgis.com/apps/Cascade/index.html?appid=de012f87befb472c9a5e7794206d1230"
          target="_blank"*/
        >
          <FontAwesomeIcon icon={faClock} />
          <FontAwesomeIcon icon={faPagelines} />
          <br />
          Series de tiempo
          <br />
          CGSM
        </a>

        <a
          onClick={(e) => {
            handleClick(e);
            Setcaracterizacion(false);
            SetEstadoMapa(false);
            SetmultipleParcelas(false);
            SetSerieTiempo(false);
          }}
        >
          <FontAwesomeIcon icon={faChartBar} />
          <FontAwesomeIcon icon={faPagelines} />
          <br />
          Indicador
          <br />
          IBIm
        </a>

        <a
          onClick={(e) => {
            handleClick(e);
            Setcaracterizacion(false);
            SetEstadoMapa(false);
            SetmultipleParcelas(false);
            SetSerieTiempo(false);
          }}
        >
          <FontAwesomeIcon icon={faChartBar} />
          <FontAwesomeIcon icon={faPagelines} />
          <br />
          Series de tiempo
          <br />
          Cispata
        </a>
        <a
        href="https://storymaps.arcgis.com/stories/128c2a7258d74383a47617b313079829" target="_blank" 
          onClick={(e) => {
            handleClick(e);
            Setcaracterizacion(false);
            SetEstadoMapa(false);
            SetmultipleParcelas(false);
            SetSerieTiempo(false);
          }}
        >
          <FontAwesomeIcon icon={faSeedling} />
          <FontAwesomeIcon icon={faPagelines} />
          <br />
          Priorización de restauración de manglar
        </a>
      </div>

      {modulo ? (
        <>
          {modulo === "Caracterización" && (
            <div className="panel StyleCaracterizacion">
              <div className="container-fluid">
                <a id="StyleA">
                  Caracterización
                  <Button
                    variant="close"
                    id="buttonClose"
                    onClick={(e) => {
                      handleClick(e);
                      Setcaracterizacion(false);
                    }}
                  />
                </a>

                <FormCaracterizacion handle={handle} />
              </div>
            </div>
          )}
          {modulo === "EstadoEstructura" && (
            <div className="panel StyleEstructura">
              <div className="container-fluid">
                <a id="StyleA">
                  Estructura
                  <Button
                    variant="close"
                    id="buttonClose"
                    onClick={(e) => {
                      handleClick(e);
                      SetEstadoMapa(false);
                    }}
                  ></Button>
                </a>
                {estrutura_modulo}
              </div>
            </div>
          )}
          {modulo === "EstadoFisicoquimico" && (
            <div className="panel StyleFisicoquimico">
              <div className="container-fluid">
                <a id="StyleA">
                  Fisicoquimico
                  <Button
                    variant="close"
                    id="buttonClose"
                    onClick={(e) => {
                      handleClick(e);
                      SetmultipleParcelas(false);
                    }}
                  ></Button>
                </a>
                {fq_modulo}
              </div>
            </div>
          )}
          {modulo === "Restauración" && (
            <div className="panel StyleRestauracion">
              <div className="container-fluid">
                <a id="StyleA">
                  Restauración
                  <Button
                    variant="close"
                    id="buttonClose"
                    onClick={(e) => {
                      handleClick(e);
                      SetEstadoMapa(false);
                    }}
                  ></Button>
                </a>
                <FormRestauracion />
              </div>
            </div>
          )}
          {modulo === "EstadoRegeneraciónNatural" && (
            <div className="panel StyleRegeneracion">
              <div className="container-fluid">
                <a id="StyleA">
                  Regeneración
                  <Button
                    variant="close"
                    id="buttonClose"
                    onClick={(e) => {
                      handleClick(e);
                      SetEstadoMapa(false);
                    }}
                  ></Button>
                </a>
                {regeneracion_modulo}
              </div>
            </div>
          )}
          {modulo === "IndicadorIBIm" && (
            <div className="panel StyleIbim">
              <div className="container-fluid">
                <a id="StyleA">
                  Indicador IBIm
                  <Button
                    variant="close"
                    id="buttonClose"
                    onClick={(e) => {
                      handleClick(e);
                      SetmultipleParcelas(false);
                    }}
                  ></Button>
                </a>
                {ibim_modulo}
              </div>
            </div>
          )}
          {modulo === "Series de tiempoCispata" && (
            <div className="panel StyleCispata">
              <div className="container-fluid">
                <a id="StyleA">
                  Series de tiempo Cispata
                  <Button
                    variant="close"
                    id="buttonClose"
                    onClick={(e) => {
                      handleClick(e);
                      SetSerieTiempo(false);
                    }}
                  ></Button>
                </a>
                <FormSeriesCispata />
              </div>
            </div>
          )}
          {modulo === "Series de tiempoCGSM" && (
            <div className="panel StyleCGSM">
              <div className="container-fluid">
                <a id="StyleA">
                  Series de tiempo CGSM
                  <Button
                    variant="close"
                    id="buttonClose"
                    onClick={(e) => {
                      handleClick(e);
                      SetSerieTiempo(false);
                    }}
                  ></Button>
                </a>
                <FormsCGSM />
              </div>
            </div>
          )}
        </>
      ) : (
        []
      )}
    </>
  );
}

export default Menu;

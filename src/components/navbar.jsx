import React, { useState } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAlignJustify } from '@fortawesome/free-solid-svg-icons';
import icon from "../assets/images/logoInvemar.png";
import ModalTabs from "./ModalTabs";
import "../assets/style/Barra.css";

function Nav() {
  const [show, setShow] = useState(false);
  const handleShow = () => setShow(true);
  const handleClose = () => setShow(false);
  return (
    <React.Fragment>
      <nav className="navbar navbar-expand-lg container-fluid">
        
        <div className="container-fluid">
          <div className="navbar-header">
            <a className="StyleAImag">
              <img className="Logo" src={icon}></img>
            </a>
            <a className="StyleTitle">
              Geovisor Sigma
            </a>
          </div>

      

          <div className="d-flex" aling="center">

            <div className="collapse navbar-collapse" id="navbarNavAltMarkup">

              <a className="nav-link StyleText" type="button" onClick={handleShow}>
                
                Acerca de
              </a>
              {/* <a className="nav-link StyleText" type="button">
                Ayuda
              </a>
              <a className="nav-link StyleText" type="button">
                Preguntas frecuentes
              </a> */}
              </div>
          </div>

          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarNavAltMarkup"
            aria-controls="navbarNavAltMarkup"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <FontAwesomeIcon icon= {faAlignJustify}/>
          </button>
        </div>
      </nav>
      <ModalTabs show={show} handleClose={handleClose}/>
    </React.Fragment>
  );
}

export default Nav;

import React,{ createContext,useState } from "react";

const MapaContext= createContext();
const {Provider} = MapaContext;


const MapaProvider = ({children}) => {

    const [data,SetData] = useState([]) /*Datos */
    const [est,SetEst] = useState([])/* estado estructura*/
    const [multipleParcelas,SetmultipleParcelas] = useState([])/* estado para multiples parcelas*/
    const [caracterizacion,Setcaracterizacion] = useState([])/* Estado Caracterización */
    const [EstRestauracion,SetEstRestauracion] = useState([])/* Estado Restauración*/
    const [capaToribio,SetCapaToribio] = useState([]) /* Estado capa Toribio */
    const [parcela,SetParcela] = useState([]) /* id_parcela */
    const [SerieTiempo,SetSerieTiempo] = useState([]) /* Estado Series de tiempo */
    const [estadoMapa,SetEstadoMapa] = useState([])
    

    const guardar =(data) => {
        SetData(data);
    }
    const guardar_estado = (est)=>{
        SetEst(est);
    }
    const guardar_estado_multipleParcelas = (multipleParcelas)=>{
        SetmultipleParcelas(multipleParcelas);
    }
    const guardar_parcela = (parcela)=> {
        SetParcela(parcela);
    }
    const guardar_estado_mapa = (estadoMapa)=> {
        SetEstadoMapa(estadoMapa);
    }

    const guardar_estado_CapaToribio = (capaToribio)=> {
        SetCapaToribio(capaToribio);
    }

    const guardar_estado_Restauracion = (EstRestauracion)=>{
        SetEstRestauracion(EstRestauracion);
    }

    const guardar_estado_caracterizacion = (caracterizacion)=>{
        Setcaracterizacion(caracterizacion);
    }

    const guardar_estado_SerieTiempo = (SerieTiempo)=>{
        SetSerieTiempo(SerieTiempo);
    }

    return (
        <Provider 
        value={{
            data,
            setData:(dato)=>guardar(dato),
            est,
            SetEst:(est)=>guardar_estado(est),
            parcela,
            SetParcela:(parcela)=>guardar_parcela(parcela),
            estadoMapa,
            SetEstadoMapa:(estadoMapa)=>guardar_estado_mapa(estadoMapa),
            multipleParcelas,
            SetmultipleParcelas:(multipleParcelas)=>guardar_estado_multipleParcelas(multipleParcelas),
            capaToribio,
            SetCapaToribio:(capaToribio)=>guardar_estado_CapaToribio(capaToribio),
            EstRestauracion,
            SetEstRestauracion:(EstRestauracion)=>guardar_estado_Restauracion(EstRestauracion),
            caracterizacion,
            Setcaracterizacion:(caracterizacion)=>guardar_estado_caracterizacion(caracterizacion),
            SerieTiempo,
            SetSerieTiempo:(SerieTiempo)=>guardar_estado_SerieTiempo(SerieTiempo),
        }}
        >
        {children}   
        </Provider>


    )
}

export {MapaContext,MapaProvider}
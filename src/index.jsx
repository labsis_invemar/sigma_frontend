import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App.jsx';
console.log(process.env.REACT_APP_URL_API)
// console.log = () => {}

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

